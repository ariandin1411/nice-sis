<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogUserClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_user_classes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_classes_id');
            $table->integer('user_id');
            $table->integer('from_classes_id')->nullable();
            $table->integer('to_classes_id')->nullable();
            $table->integer('insert_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_user_classes');
    }
}
