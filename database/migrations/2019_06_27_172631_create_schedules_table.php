<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subject');
            $table->integer('user_id');
            $table->integer('time');
            $table->string('day_name');
            $table->string('schedule_type')->nullable();
            $table->integer('year_id');
            $table->integer('semester')->nullable();
            $table->integer('class_id');
            $table->tinyInteger('active')->default(1);
            $table->integer('insert_by');
            $table->integer('update_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
