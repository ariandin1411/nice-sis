<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentDetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_dets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_id')->nullable();
            $table->integer('prod_id')->nullable();
            $table->string('prod_nm')->nullable();
            $table->decimal('sale_prc',16,2)->nullable();
            $table->integer('insert_by')->nullable();
            $table->integer('update_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_dets');
    }
}
