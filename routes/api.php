<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResources(['users' => 'API\UserController']);
Route::apiResources(['roles' => 'API\RoleController']);
Route::apiResources(['user-roles' => 'API\UserRoleController']);
Route::get('students', 'API\UserController@showStudent');
Route::get('users-all', 'API\UserController@all');
Route::get('findUsers', 'API\UserController@search');
Route::get('find-students', 'API\UserController@searchStudent');
Route::get('user-parent/{user_id}', 'API\UserController@userParent');
Route::put('user-parent-update/{user_id}', 'API\UserController@userParentUpdate');
Route::get('user-sch-sel', 'API\UserController@showTeachersSch');
Route::get('find-all', 'API\UserController@searchAll');
Route::put('change-classes', 'API\UserController@changeClasses');
Route::get('student-logs/{user_id}', 'API\UserController@selectLog');
Route::get('provinces', 'API\CompanyController@province');
Route::get('regencies', 'API\CompanyController@regency');
Route::get('districts', 'API\CompanyController@district');
Route::get('findCompany', 'API\CompanyController@findCompany');
Route::get('find-company-by-user', 'API\CompanyController@findCompanyUser');
Route::post('store-company-user', 'API\CompanyController@storeCompanyUser');
Route::put('update-company-user/{id}', 'API\CompanyController@updateCompanyUser');
Route::delete('delete-company-user/{id}', 'API\CompanyController@deleteCompanyUser');
Route::get('role-by-user/{id}', 'API\UserRoleController@rolePerUser');
Route::get('findClasses', 'API\ClassesController@findClasses');
Route::get('classes-sch-sel', 'API\ClassesController@showClassesSch');
Route::get('invoice-by-id/{id}','API\InvoiceController@showInvoice');
Route::get('presents-list','API\PresentsController@presentsList');
Route::post('payments/noti','API\PaymentController@noti')->name('db-process-url');
Route::get('payments/payment-pg','API\PaymentController@paymentPg')->name('payment-pg');
Route::apiResources(['companies' => 'API\CompanyController']);
Route::apiResources(['classes' => 'API\ClassesController']);
Route::apiResources(['years' => 'API\YearController']);
Route::apiResources(['subjects' => 'API\SubjectsController']);
Route::apiResources(['schedules' => 'API\ScheduleController']);
Route::apiResources(['teacher-class' => 'API\TeacherClassesController']);
Route::apiResources(['products' => 'API\ProductController']);
Route::apiResources(['times' => 'API\TimeController']);
Route::apiResources(['invoices' => 'API\InvoiceController']);
Route::apiResources(['scores' => 'API\ScoreController']);
Route::apiResources(['presents' => 'API\PresentsController']);
Route::get('history-users', 'API\UserController@showHistoryUsers');
Route::apiResources(['payments' => 'API\PaymentController']);
