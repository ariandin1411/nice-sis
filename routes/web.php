<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('mail/send-va-created', 'MailController@send')->name('end-va-created');

Route::get('user-pdf-view', 'API\UserController@userPdfView')->name('user-pdf-view');
Route::get('user-excel-view', 'API\UserController@userExcelView')->name('user-excel-view');
Route::get('students-excel-view', 'API\UserController@studentsExcelView')->name('user-excel-view');
Route::get('teacherclasses-excel-view', 'API\TeacherClassesController@teacherClassesExcelView')->name('teacherclasses-excel-view');

Route::get('invoice', function(){
  return view('invoice');
});

Route::get('{path}', 'HomeController@index')->where('path', '([A-z\d-\/_.]+)?');
