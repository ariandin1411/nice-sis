
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import { Form, HasError, AlertError } from 'vform'
import VueRouter from 'vue-router'
import moment from 'moment'
import VueProgressBar from 'vue-progressbar'
import Swal from 'sweetalert2'
import vSelect from 'vue-select'
import Gate from './Gate'
import StaticConfig from './StaticConfig'
import VueNumeric from 'vue-numeric'

Vue.prototype.$gate = new Gate(window.user)
Vue.prototype.$static = new StaticConfig()

window.Form = Form;
window.Swal = Swal;
window.Fire = new Vue();

const toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
})

window.toast = toast

Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)
Vue.component('v-select', vSelect)

Vue.use(VueRouter)

let routes = [
    { path: '/', component: require('./components/dashboard.vue').default },
    { path: '/dashboard', component: require('./components/dashboard.vue').default },
    { path: '/users', component: require('./components/users.vue').default },
    { path: '/users-all', component: require('./components/users-all.vue').default },
    { path: '/student', component: require('./components/student.vue').default },
    { path: '/student/view/:userid', component: require('./components/student-view.vue').default },
    { path: '/student-change_class_up', component: require('./components/studentChangeClass.vue').default },
    { path: '/roles', component: require('./components/roles.vue').default },
    { path: '/times', component: require('./components/times.vue').default },
    { path: '/user-roles', component: require('./components/user-roles.vue').default },
    { path: '/users/:userid', component: require('./components/users.vue').default },
    { path: '/users/view/:userid', component: require('./components/users-view.vue').default },
    { path: '/companies', component: require('./components/companies.vue').default },
    { path: '/classes', component: require('./components/classes.vue').default },
    { path: '/developer', component: require('./components/developer.vue').default },
    { path: '/profile', component: require('./components/profile.vue').default },
    { path: '/products', component: require('./components/product.vue').default },
    { path: '/years', component: require('./components/years.vue').default },
    { path: '/teacher-class', component: require('./components/teacher-class.vue').default },
    { path: '/subjects', component: require('./components/subjects.vue').default },
    { path: '/schedules', component: require('./components/schedules.vue').default },
    { path: '/schedules-edit', component: require('./components/schedules-edit.vue').default },
    { path: '/invoices', component: require('./components/invoices.vue').default, name: 'invoices' },
    { path: '/invoices-add/:id', component: require('./components/invoices-add.vue').default },
    { path: '/invoices-print/:id', component: require('./components/invoices-print.vue').default },
    { path: '/history-users', component: require('./components/history-users.vue').default },
    { path: '/scores', component: require('./components/scores.vue').default },
    { path: '/presents', component: require('./components/presents.vue').default },
    { path: '/presents-list', component: require('./components/presents-list.vue').default },
    { path: '/payments', component: require('./components/payments.vue').default },
    { path: '/payments-success', component: require('./components/payments-success.vue').default, name: 'payments-success' },
    { path: '*', component: require('./components/notFound.vue').default }
]

const router = new VueRouter({
    routes // short for `routes: routes`
})

Vue.filter('upText', function (text) {
    if(text){
        return text.charAt(0).toUpperCase() + text.slice(1);
    }
    return '';
});

Vue.filter('splitText1', function (text) {
    if(text){
        return text.split(" : ")[0];
    }
    return '';
});

Vue.filter('splitText2', function (text) {
    if(text){
        return text.split(" : ")[1];
    }
    return '';
});

Vue.filter('myDate', function (created) {
    // return created.moment().format('MMMM Do YYYY, h:mm:ss a');
    if(created){
        return moment(created).format('DD MMMM YYYY');
    }
    return '';
});

Vue.filter('myDateInv', function (created) {
    if(created){
        return moment(created).format('MMMM YYYY');
    }

    return '';
    
});

Vue.filter('myDateInv2', function (created) {
    if(created){
        return moment(created).format('DD MMMM YYYY');
    }

    return '';
    
});

Vue.filter('myDateInv3', function (created) {
    if(created){
        return created.substring(0, 2)+':'+created.substring(2, 4);
    }

    return '';
    
});

Vue.filter('fpresent', function (created) {
    if( created == 'present' ){
        return 'Hadir';
    }else if( created == 'not present' ){
        return 'Alfa';
    }else if( created == 'sicks' ){
        return 'Sakit';
    }else if( created == 'permission' ){
        return 'Izin';
    }

    return '-';
    
});

Vue.filter('amountFormat', function (num) {
    let numToInt =  parseInt(num)
    // return numToInt
    if(num){
        return 'Rp ' + numToInt.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    return 'Rp 0';
    
});

Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
})

Vue.use(VueNumeric)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

Vue.component(
    'not-found',
    require('./components/notFound.vue').default
);

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));

Vue.mixin({
  data: function() {
    return {
      get dayNameOptions() {
        let dayNameOptions = [
                                { code: "senin", label: "Senin" },
                                { code: "selasa", label: "Selasa" },
                                { code: "rabu", label: "Rabu" },
                                { code: "kamis", label: "Kamis" },
                                { code: "jumat", label: "Jumat" },
                                { code: "sabtu", label: "Sabtu" },
                                { code: "minggu", label: "Minggu" },
                              ]
        return dayNameOptions;
      },
      get scheduleType() {
          let scheduleType = [
                                { code: "reguler", label: "Reguler" },
                                { code: "informal", label: "Informal" },
                                { code: "exam", label: "Exam" },
                              ]
        return scheduleType;
      },

      get presentsDesc() {
          let presentsDesc = [
                                { code: "present", label: "Hadir" },
                                { code: "not present", label: "Tidak Hadir" },
                                { code: "sicks", label: "Sakit" },
                                { code: "permission", label: "Izin" },
                              ]
        return presentsDesc;
      }, 

      get bankListOptions() {
        let bankListOptions = [
                                { code: "BMRI", label: "Mandiri" },
                                { code: "BNIN", label: "BNI" },
                                { code: "BRIN", label: "BRI" },
                                { code: "CENA", label: "BCA" },
                                { code: "BBBA", label: "Permata" },
                                { code: "BNIA", label: "CIMB Niaga" },
                                { code: "BDIN", label: "Danamon" },
                              ]
        return bankListOptions;
      }, 
    }
  }
})

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
    router,
    data: {
        search: '',
    },
    methods: {
        searchit() {
            Fire.$emit('searching')
        }
    }
});
