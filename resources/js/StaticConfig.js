export default class StaticConfig{
  payMethod(){
    var data = [
              { code: "02", label: "Transfer Bank" },
              { code: "03", label: "Indomaret / Alfamart" },
              { code: "05", label: "E-Wallet / Uang Elektronik (OVO)" },
            ];

    return data;
  }

  cvsList(){
    var data = [
              { code: "indo", label: "Indomaret" },
              { code: "alma", label: "Alfamart" },
            ];

    return data;
  }

  genderList(){
    var data = [
              { code: "M", label: "Laki - Laki" },
              { code: "F", label: "Perempuan" },
            ];

    return data;
  }

  religionList(){
    var data = [
              { code: 'islam', label: 'Islam' },
              { code: 'kristen protestan', label: 'Kristen Protestan' },
              { code: 'katolik', label: 'Katolik' },
              { code: 'hindu', label: 'Hindu' },
              { code: 'buddha', label: 'Buddha' },
              { code: 'kong hu cu', label: 'Kong Hu Cu' },
            ];

    return data;
  }

  dataPerPage(){
    var data = [10, 20, 50, 100];

    return data;
  }

  ewalletList(){
    var data = [
              { code: "ovoe", label: "OVO" },
            ];

    return data;
  }

  monthNameOptions(){
    var data = [
                { code: "01", label: "Januari" },
                { code: "02", label: "Februari" },
                { code: "03", label: "Maret" },
                { code: "04", label: "April" },
                { code: "05", label: "Mei" },
                { code: "06", label: "Juni" },
                { code: "07", label: "Juli" },
                { code: "08", label: "Agustus" },
                { code: "09", label: "September" },
                { code: "10", label: "Oktober" },
                { code: "11", label: "November" },
                { code: "12", label: "Desember" },
              ];

    return data;
  }

  bankCdToName(bankCd){
    var bankName = '';
    if(bankCd == 'BMRI'){
      bankName = 'Bank Mandiri';
    }else if(bankCd == 'BNIN'){
      bankName = 'BNI 46';
    }else if(bankCd == 'BRIN'){
      bankName = 'BRI';
    }else if(bankCd == 'CENA'){
      bankName = 'BCA';
    }else if(bankCd == 'BBBA'){
      bankName = 'Bank Permata';
    }else if(bankCd == 'BNIA'){
      bankName = 'Bank CIMB Niaga';
    }else if(bankCd == 'BDIN'){
      bankName = 'Bank Danamon';
    }

    return bankName
  }

  bankCdToPayWay(bankCd){
    var content = '';
    if(bankCd == 'BMRI'){
      content = '<h3>ATM Mandiri</h3>';
      content += '<ul>';
      content += '<li>Input kartu ATM dan PIN Anda</li>';
      content += '<li>Pilih Menu Bayar/Beli</li>';
      content += '<li>Pilih Lainnya</li>';
      content += '<li>Pilih Multi Payment</li>';
      content += '<li>Input 70014 sebagai Kode Institusi</li>';
      content += '<li>Input Virtual Account Number, misal. 70014XXXXXXXXXXX</li>';
      content += '<li>Pilih Benar</li>';
      content += '<li>Pilih Ya</li>';
      content += '<li>Pilih Ya</li>';
      content += '<li>Ambil bukti bayar Anda</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking Mandiri</h3>';
      content += '<ul>';
      content += '<li>Login Mobile Banking</li>';
      content += '<li>Pilih Bayar</li>';
      content += '<li>Pilih Multi Payment</li>';
      content += '<li>Input Transferpay sebagai Penyedia Jasa</li>';
      content += '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>';
      content += '<li>Pilih Lanjut</li>';
      content += '<li>Input OTP and PIN</li>';
      content += '<li>Pilih OK</li>';
      content += '<li>Bukti bayar ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Internet Banking Mandiri</h3>';
      content += '<ul>';
      content += '<li>Login Internet Banking</li>';
      content += '<li>Pilih Bayar</li>';
      content += '<li>Pilih Multi Payment</li>';
      content += '<li>Input Transferpay sebagai Penyedia Jasa</li>';
      content += '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX sebagai Kode Bayar</li>';
      content += '<li>Ceklis IDR</li>';
      content += '<li>Klik Lanjutkan</li>';
      content += '<li>Bukti bayar ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';
    }else if(bankCd == 'BNIN'){
      content = '<h3>ATM BNI</h3>';
      content += '<ul>';
      content += '<li>Masukkan Kartu Anda.</li>';
      content += '<li>Pilih Bahasa</li>';
      content += '<li>Masukkan PIN ATM Anda.</li>';
      content += '<li>Pilih Menu Lainnya.</li>';
      content += '<li>Pilih Transfer.</li>';
      content += '<li>Pilih Jenis rekening yang akan Anda gunakan (Contoh; Dari Rekening Tabungan).</li>';
      content += '<li>Pilih Virtual Account Billing</li>';
      content += '<li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX).</li>';
      content += '<li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
      content += '<li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>';
      content += '<li>Transaksi Anda telah selesai.</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking BNI</h3>';
      content += '<ul>';
      content += '<li>Akses BNI Mobile Banking dari handphone kemudian masukkan user ID dan password.</li>';
      content += '<li>Pilih menu Transfer.</li>';
      content += '<li>Pilih menu Virtual Account Billing kemudian pilih rekening debet.</li>';
      content += '<li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX) pada menu input baru.</li>';
      content += '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>';
      content += '<li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
      content += '<li>Konfirmasi transaksi dan masukkan Password Transaksi.</li>';
      content += '<li>Pembayaran Anda Telah Berhasil.</li>';
      content += '</ul>';

      content += '<h3>iBank Personal BNI</h3>';
      content += '<ul>';
      content += '<li>Ketik alamat https://ibank.bni.co.id kemudian klik Enter.</li>';
      content += '<li>Masukkan User ID dan Password.</li>';
      content += '<li>Pilih menu Transfer.</li>';
      content += '<li>Pilih Virtual Account Billing.</li>';
      content += '<li>Kemudian masukan Nomor Virtual Account Anda (contoh: 123456789012XXXX) yang hendak dibayarkan. Lalu pilih rekening debet yang akan digunakan. Kemudian tekan lanjut</li>';
      content += '<li>Kemudin tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
      content += '<li>Masukkan Kode Otentikasi Token.</li>';
      content += '<li>Pembayaran Anda telah berhasil.</li>';
      content += '</ul>';
    }else if(bankCd == 'BRIN'){
      content = '<h3>ATM BRI</h3>';
      content += '<ul>';
      content += '<li>Input kartu ATM dan PIN Anda</li>';
      content += '<li>Pilih Menu Transaksi Lain</li>';
      content += '<li>Pilih Menu Pembayaran</li>';
      content += '<li>Pilih Menu Lain-lain</li>';
      content += '<li>Pilih Menu BRIVA</li>';
      content += '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
      content += '<li>Pilih Ya</li>';
      content += '<li>Ambil bukti bayar anda</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking BRI</h3>';
      content += '<ul>';
      content += '<li>Login BRI Mobile</li>';
      content += '<li>Pilih Mobile Banking BRI</li>';
      content += '<li>Pilih Menu Pembayaran</li>';
      content += '<li>Pilih Menu BRIVA</li>';
      content += '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
      content += '<li>Masukkan Nominal misal. 10000</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Masukkan PIN Mobile</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar akan dikirim melalui sms</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Internet Banking BRI</h3>';
      content += '<ul>';
      content += '<li>Login Internet Banking</li>';
      content += '<li>Pilih Pembayaran</li>';
      content += '<li>Pilih BRIVA</li>';
      content += '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Masukkan Password</li>';
      content += '<li>Masukkan mToken</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar akan ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';
    }else if(bankCd == 'CENA'){
      content = '<h3>ATM BCA</h3>';
      content += '<ul>';
      content += '<li>Input kartu ATM dan PIN Anda</li>';
      content += '<li>Pilih Menu Transaksi Lainnya</li>';
      content += '<li>Pilih Transfer</li>';
      content += '<li>Pilih Ke rekening BCA Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 123456789012XXXX</li>';
      content += '<li>Pilih Benar</li>';
      content += '<li>Pilih Ya</li>';
      content += '<li>Ambil bukti bayar Anda</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking BCA</h3>';
      content += '<ul>';
      content += '<li>Login Mobile Banking</li>';
      content += '<li>Pilih m-Transfer</li>';
      content += '<li>Pilih BCA Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>';
      content += '<li>Klik Send</li>';
      content += '<li>Informasi Virtual Account akan ditampilkan</li>';
      content += '<li>Klik OK</li>';
      content += '<li>Input PIN Mobile Banking</li>';
      content += '<li>Bukti bayar ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Internet Banking BCA</h3>';
      content += '<ul>';
      content += '<li>Login Internet Banking</li>';
      content += '<li>Pilih Transaksi Dana</li>';
      content += '<li>Pilih Transfer Ke BCA Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Input Respon KeyBCA Appli 1</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';
    }else if(bankCd == 'BBBA'){
      content = '<h3>ATM Permata</h3>';
      content += '<ul>';
      content += '<li>Input kartu ATM dan PIN Anda</li>';
      content += '<li>Pilih Menu Transaksi Lainnya</li>';
      content += '<li>Pilih Pembayaran</li>';
      content += '<li>Pilih Pembayaran Lain-lain</li>';
      content += '<li>Pilih Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX</li>';
      content += '<li>Select Benar</li>';
      content += '<li>Select Ya</li>';
      content += '<li>Ambil bukti bayar anda</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking Permata</h3>';
      content += '<ul>';
      content += '<li>Login Mobile Banking</li>';
      content += '<li>Pilih Pembayaran Tagihan</li>';
      content += '<li>Pilih Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX sebagai No. Virtual Account</li>';
      content += '<li>Input Nominal misal. 10000</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Input Token</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Internet Banking BCA</h3>';
      content += '<ul>';
      content += '<li>Login Internet Banking</li>';
      content += '<li>Pilih Pembayaran Tagihan</li>';
      content += '<li>Virtual Account</li>';
      content += '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX sebagai No. Virtual Account</li>';
      content += '<li>Input Nominal misal. 10000</li>';
      content += '<li>Input Token</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar akan ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';
    }else if(bankCd == 'BNIA'){
      content = '<h3>ATM CIMB Niaga</h3>';
      content += '<ul>';
      content += '<li>Input kartu ATM dan PIN Anda</li>';
      content += '<li>Pilih Menu Pembayaran</li>';
      content += '<li>Pilih Menu Lanjut</li>';
      content += '<li>Pilih Menu Virtual Account</li>';
      content += '<li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>';
      content += '<li>Pilih Proses</li>';
      content += '<li>Data Virtual Account akan ditampilkan</li>';
      content += '<li>Pilih Proses</li>';
      content += '<li>Ambil bukti bayar anda</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Mobile Banking CIMB Niaga</h3>';
      content += '<ul>';
      content += '<li>Login Go Mobile</li>';
      content += '<li>Pilih Menu Transfer</li>';
      content += '<li>Pilih Menu Transfer ke CIMB Niaga Lain</li>';
      content += '<li>Pilih Sumber Dana yang akan digunakan</li>';
      content += '<li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>';
      content += '<li>Masukkan Nominal misal. 10000</li>';
      content += '<li>Klik Lanjut</li>';
      content += '<li>Data Virtual Account akan ditampilkan</li>';
      content += '<li>Masukkan PIN Mobile</li>';
      content += '<li>Klik Konfirmasi</li>';
      content += '<li>Bukti bayar akan dikirim melalui sms</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';

      content += '<h3>Internet Banking CIMB Niaga</h3>';
      content += '<ul>';
      content += '<li>Login Internet Banking</li>';
      content += '<li>Pilih Pembayaran Tagihan</li>';
      content += '<li>Rekening Sumber - Pilih yang akan Anda digunakan</li>';
      content += '<li>Jenis Pembayaran - Pilih Virtual Account</li>';
      content += '<li>Untuk Pembayaran - Pilih Masukkan Nomor Virtual Account</li>';
      content += '<li>Nomor Rekening Virtual, misal. 5919XXXXXXXXXXXX</li>';
      content += '<li>Isi Remark Jika diperlukan</li>';
      content += '<li>Klik Lanjut</li>';
      content += '<li>Data Virtual Account akan ditampilkan</li>';
      content += '<li>Masukkan mPIN</li>';
      content += '<li>Klik Kirim</li>';
      content += '<li>Bukti bayar akan ditampilkan</li>';
      content += '<li>Selesai</li>';
      content += '</ul>';
    }else if(bankCd == 'BDIN'){
      content = '<h3>ATM Danamon</h3>';
      content += '<ul>';
      content += '<li>Input PIN ATM Anda</li>';
      content += '<li>Pilih Menu Pembayaran >>> Virtual Account Masukan nomor Virtual Account</li>';
      content += '<li>Masukkan Nominal</li>';
      content += '<li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi</li>';
      content += '</ul>';

      content += '<h3>Aplikasi D-Mobile Danamon</h3>';
      content += '<ul>';
      content += '<li>Login pada Aplikasi D-Mobile</li>';
      content += '<li>Pilih menu Virtual Account</li>';
      content += '<li>Masukan 16 digit nomor virtual account</li>';
      content += '<li>Masukan Nominal</li>';
      content += '<li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi.</li>';
      content += '</ul>';
    }

    return content;
  }
}
