
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Sistem Informasi Sekolah</title>

  <link rel="stylesheet" type="text/css" href="/css/app.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper" id="app">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <div class="form-inline ml-3">
      <div class="input-group input-group-sm">
        {{-- <input class="form-control form-control-navbar" type="search" 
        placeholder="Search" aria-label="Search" v-model="search"  --}}
        {{-- @keyup.enter="searchit" --}}
        {{-- @keyup="searchit"> --}}
        {{-- <div class="input-group-append">
          <button class="btn btn-navbar" @click="search">
            <i class="fa fa-search"></i>
          </button>
        </div> --}}
      </div>
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('img/nice.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light"> Sistem Info. Sekolah </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('img/customer-support.png') }}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{ Auth::user()->first_name }} {{ Auth::user()->last_name }} </a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <router-link to="/dashboard" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt text-blue"></i>
              <p>
                Dasboard
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </router-link>
          </li>
            {{-- @if(Auth()->user()->role == 'admin' || Auth()->user()->role == 'guru') --}}
            @if(Auth()->user()->role == 'admin')
            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-cog text-green"></i>
                <p>
                  Data Guru dan Siswa
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/users-all" class="nav-link">
                    <i class="fas fa-users-cog nav-icon"></i>
                    <p>Semua Data Guru dan Siswa</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/users/{{ Auth::user()->id }}" class="nav-link">
                    <i class="fas fa-users nav-icon"></i>
                    <p>Guru</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <!-- <a href="#" class="nav-link"> -->
                  <router-link to="/teacher-class" class="nav-link">
                    <i class="fas fa-user-shield nav-icon"></i>
                    <p>Wali Kelas</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/student" class="nav-link">
                    <i class="fas fa-walking nav-icon"></i>
                    <p>Siswa</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <router-link to="/student-change_class_up" class="nav-link">
                    <i class="fas fa-user-friends nav-icon"></i>
                    <p>Siswa Naik Kelas</p>
                  </router-link>
                </li>
              </ul>
            </li>

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-save text-cyan"></i>
                <p>
                  Master Data
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/roles" class="nav-link">
                    <i class="fas fa-meh-rolling-eyes nav-icon"></i>
                    <p>Master Role</p>
                  </router-link>
                </li>
                <li class="nav-item">
                  <!-- <a href="#" class="nav-link"> -->
                  <router-link to="/user-roles" class="nav-link">
                    <i class="fa fa-user-tag nav-icon"></i>
                    <p>User Role</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <!-- <a href="#" class="nav-link"> -->
                  <router-link to="/companies" class="nav-link">
                    <i class="fas fa-building nav-icon"></i>
                    <p>Daftar Sekolah</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/classes" class="nav-link">
                    <i class="fas fa-gopuram nav-icon"></i>
                    <p>Daftar Kelas</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/years" class="nav-link">
                    <i class="fas fa-calendar nav-icon"></i>
                    <p>Tahun Ajaran</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/subjects" class="nav-link">
                    <i class="fas fa-calendar-alt nav-icon"></i>
                    <p>Mata Pelajaran</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/products" class="nav-link">
                    <i class="fas fa-calendar-alt nav-icon"></i>
                    <p>Product</p>
                  </router-link>
                  <!-- </a> -->
                </li>
                <li class="nav-item">
                  <router-link to="/times" class="nav-link">
                    <i class="fas fa-clock nav-icon"></i>
                    <p>Jam Pelajaran</p>
                  </router-link>
                </li>
              </ul>
            </li>

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-info-circle text-teal"></i>
                <p>
                  Kurikulum
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <router-link to="/schedules" class="nav-link">
                    <i class="fas fa-book nav-icon"></i>
                    <p>Mata Pelajaran</p>
                  </router-link>
                  <router-link to="/schedules-edit" class="nav-link">
                    <i class="fas fa-user-edit nav-icon"></i>
                    <p>Edit Mata Pelajaran</p>
                  </router-link>
                  <router-link to="/scores" class="nav-link">
                    <i class="fas fa-star nav-icon"></i>
                    <p>Nilai</p>
                  </router-link>
                  <router-link to="/presents" class="nav-link">
                    <i class="fas fa-clipboard-list nav-icon"></i>
                    <p>Input Absensi</p>
                  </router-link>
                  <router-link to="/presents-list" class="nav-link">
                    <i class="fas fa-list-ol nav-icon"></i>
                    <p>Daftar Absensi</p>
                  </router-link>
                </li>
              </ul>
            </li>

            @endif

            @if(Auth()->user()->role == 'admin' || Auth()->user()->role == 'guru' || Auth()->user()->role == 'siswa')

            <li class="nav-item has-treeview">
              <a href="#" class="nav-link">
                <i class="nav-icon fas fa-receipt text-blue"></i>
                <p>
                  Biaya Administrasi
                  <i class="right fa fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">

                  @if(Auth()->user()->role == 'admin')
                    <router-link to="/invoices" class="nav-link">
                      <i class="fas fa-file-invoice nav-icon"></i>
                      <p>Bayaran Bulanan</p>
                    </router-link>
                  @endif
                  
                  @if(Auth()->user()->role == 'admin' || Auth()->user()->role == 'siswa')
                    <router-link to="/payments" class="nav-link">
                      <i class="fas fa-address-card nav-icon"></i>
                      <p>Bayar Bulanan</p>
                    </router-link>

                    <router-link to="/payments-success" class="nav-link">
                      <i class="fas fa-address-book nav-icon"></i>
                      <p>Data Bayar Bulanan</p>
                    </router-link>
                  @endif

                </li>
              </ul>
            </li>
            
            @endif

            @if(Auth()->user()->role == 'admin')
              <li class="nav-item">
                <router-link to="/developer" class="nav-link">
                  <i class="nav-icon fas fa-cogs text-yellow"></i>
                  <p>
                    Developer
                    {{-- <span class="right badge badge-danger">New</span> --}}
                  </p>
                </router-link>
              </li>
            @endif

            
          

          <li class="nav-item">
            <router-link to="/profile" class="nav-link">
              <i class="nav-icon fas fa-user text-teal"></i>
              <p>
                Profil
                {{-- <span class="right badge badge-danger">New</span> --}}
              </p>
            </router-link>
          </li>

          <li class="nav-item">
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" 
            class="nav-link">
              <i class="nav-icon fas fa-power-off text-red"></i>
              <p>
                {{ __('Logout') }}
                {{-- <span class="right badge badge-danger">New</span> --}}

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">

        <router-view></router-view>
        <vue-progress-bar></vue-progress-bar>
        
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      SIS 88
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2019-2019 <a href="https://adminlte.io">db_duabelas@yahoo.com</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

@auth
  @php
    $userData = auth()->user();
    $userData->role = auth()->user()->role;
  @endphp
  <script type="text/javascript">
    window.user = @json($userData)
  </script>
@endauth

<!-- REQUIRED SCRIPTS -->

<script type="text/javascript" src="/js/app.js"></script>
</body>
</html>
