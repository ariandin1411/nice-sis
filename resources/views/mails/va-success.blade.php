<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Hello <i>{{ $va->receiver }}</i>,</h2>
        Pembayaran Sukses,
        <p>Terimakasih telah membayar tagihan anda.</p> 

        <table class="table">
          <tr>
            <td>Bank</td>
            <td>: {{ $va->bankCd }}</td>
          </tr>

          <tr>
            <td>Atas Nama</td>
            <td>: {{ $va->billingNm }}</td>
          </tr>

          <tr>
            <td>tXid</td>
            <td>: {{ $va->tXid }}</td>
          </tr>

          <tr>
            <td>Total Bayar</td>
            <td>: Rp. {{ number_format($va->amt) }}</td>
          </tr>

          <tr>
            <td>No Va</td>
            <td>: {{ $va->vacctNo }}</td>
          </tr>

        </table>
        <br />
        Thank You,
        <br/>
        <i>{{ $va->sender }}</i>
      </div>
    </div>
  </div>
</body>
</html>
