Hello {{ $va->receiver }},
Pembayaran Sukses,
Terimakasih telah membayar tagihan anda.

Bank : {{ $va->bankCd }}
Atas Nama : {{ $va->billingNm }}
tXid : {{ $va->tXid }}
Total Bayar : Rp. {{ number_format($va->amt) }}
No Va : {{ $va->vacctNo }}

Thank You,
{{ $va->sender }}
