<!DOCTYPE html>
<html>
<head>
  <title></title>
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>

  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h2>Hello <i>{{ $va->receiver }}</i>,</h2>
        <p>Tagihan anda untuk bayar bulanan sekolah berhasil terbuat,</p> 
        <p>silahkan bayar tagihan anda dengan data di bawah.</p>

        <table class="table">
          <tr>
            <td>Bank</td>
            <td>: {{ $va->bankCd }}</td>
          </tr>

          <tr>
            <td>Atas Nama</td>
            <td>: {{ $va->billingNm }}</td>
          </tr>

          <tr>
            <td>tXid</td>
            <td>: {{ $va->tXid }}</td>
          </tr>

          <tr>
            <td>Total Bayar</td>
            <td>: Rp. {{ number_format($va->amt) }}</td>
          </tr>

          <tr>
            <td>No Va</td>
            <td>: {{ $va->vacctNo }}</td>
          </tr>

          <tr>
            <td>Valid Samapai</td>
            <td>: {{ substr($va->vacctValidDt, 0, 2) }}-{{ substr($va->vacctValidDt, 4, 2) }}-{{ substr($va->vacctValidDt, 6, 2) }}, {{ substr($va->vacctValidTm, 0, 2) }}:{{ substr($va->vacctValidTm, 2, 2) }}</td>
          </tr>
        </table>
        
        <h2>Cara Pembayaran</h2>,
        {!! $va->payWay !!}
        <br />
        Thank You,
        <br/>
        <i>{{ $va->sender }}</i>
      </div>
    </div>
  </div>
</body>
</html>
