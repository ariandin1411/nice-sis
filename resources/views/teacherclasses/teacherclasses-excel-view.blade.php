<!DOCTYPE html>
<html>
<head>
  <title>Data Guru - {{ $title }}</title>
  <style type="text/css">
    th, td{
      font-size: 12px;
      padding: 6px;
    }
    td{
      font-size: 11px;
      padding: 4px;
    }
  </style>
</head>
<body>
  <div style="text-align: center;">
    <table style="width: 100%;" border="1">
      <thead>
        <tr>
          <th colspan="5" align="center"><h3>Data Wali Kelas - {{ $title }}</h3></th>
        </tr>
        <tr>
          <th colspan="5" align="center">&nbsp;</th>
        </tr>
        <tr style="background: #ccc;">
          <th>No</th>
          <th>N.I.G</th>
          <th>Nama</th>
          <th>Kelas</th>
          <th>Tahun Ajaran</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($teacherData as $key => $element)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ strval($element->nig) }}</td>
            <td>{{ $element->first_name }} {{ $element->last_name }}</td>
            <td>{{ $element->class_name }}{{ $element->major }} - {{ $element->class_sub_name }}</td>
            <td>{{ $element->year_name }}</td>
          </tr>      
        @endforeach
      </tbody>
    </table>
  </div>
</body>
</html>
