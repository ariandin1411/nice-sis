<!DOCTYPE html>
<html>
<head>
  <title>Data Guru - {{ $title }}</title>
  <style type="text/css">
    th, td{
      font-size: 12px;
      padding: 6px;
    }
    td{
      font-size: 11px;
      padding: 4px;
    }
  </style>
</head>
<body>
  <div style="text-align: center;">
    <h3>Data Guru - {{ $title }}</h3>
    <table style="width: 100%;" border="1">
      <tr style="background: #ccc;">
        <th>No</th>
        <th>N.I.G</th>
        <th>Nama</th>
        <th>Email</th>
        <th>No.Handphone</th>
        <th>Tahun Masuk</th>
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
      </tr>

      @foreach ($teacherData as $key => $element)
        <tr>
          <td>{{ $key + 1 }}</td>
          <td>{{ $element->nig }}</td>
          <td>{{ $element->first_name }} {{ $element->last_name }}</td>
          <td>{{ $element->email }}</td>
          <td>{{ $element->phone_number }}</td>
          <td>{{ date('d F Y', strtotime($element->year_of_entry)) }}</td>
          <td>{{ $element->born_at }}</td>
          <td>{{ date('d F Y', strtotime($element->birth_date)) }}</td>
        </tr>      
      @endforeach
    </table>
  </div>
</body>
</html>
