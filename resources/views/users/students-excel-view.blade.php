<!DOCTYPE html>
<html>
<head>
  <title>Data Siswa - {{ $title }}</title>
  <style type="text/css">
    th, td{
      font-size: 12px;
      padding: 6px;
    }
    td{
      font-size: 11px;
      padding: 4px;
    }
  </style>
</head>
<body>
  <div style="text-align: center;">
    <table style="width: 100%;" border="1">
      <thead>
        <tr>
          <th colspan="9" align="center"><h3>Data Siswa - {{ $title }}</h3></th>
        </tr>
        <tr>
          <th colspan="9" align="center">&nbsp;</th>
        </tr>
        <tr style="background: #ccc;">
          <th>No</th>
          <th>N.I.S</th>
          <th>Kelas</th>
          <th>Nama</th>
          <th>Email</th>
          <th>No.Handphone</th>
          <th>Tahun Masuk</th>
          <th>Tempat Lahir</th>
          <th>Tanggal Lahir</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($studentsData as $key => $element)
          <tr>
            <td>{{ $key + 1 }}</td>
            <td>{{ strval($element->nis) }}</td>
            <td>{{ $element->class_name }}{{ $element->major }} - {{ $element->class_sub_name }}</td>
            <td>{{ $element->first_name }} {{ $element->last_name }}</td>
            <td>{{ $element->email }}</td>
            <td>{{ $element->phone_number }}</td>
            <td>{{ date('d F Y', strtotime($element->year_of_entry)) }}</td>
            <td>{{ $element->born_at }}</td>
            <td>{{ date('d F Y', strtotime($element->birth_date)) }}</td>
          </tr>      
        @endforeach
      </tbody>
    </table>
  </div>
</body>
</html>
