<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Role;
use App\Models\UserRole;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(strtolower(Auth()->user()->role) == 'admin' || strtolower(Auth()->user()->role) == 'guru' 
            || strtolower(Auth()->user()->role) == 'siswa'){
          return $next($request);
        }
        Auth::logout();
        return redirect('/login')->with('error', 'Anda tidak mempunyai akses masuk halaman');
    }
}
