<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Year;

class YearController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $years = Year::orderBy('year_name');

        if($cari = \Request::get('cariThnAjaran')){
            $years = $years->where(function($query) use ($cari) {
                $query->where('year_name', 'like', "%$cari%");
            });
        }

        if( \Request::get('perPage') ){
            $perPage = \Request::get('perPage');
        }

        $years = $years->paginate($perPage);    
        return $years;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'year_name' => ['required', 'string', 'max:191'],
        ]);

        return Year::create([
            'year_name' => $request->year_name,
            'active' => 0,
            'insert_by' => Auth::user()->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $year = Year::find($id);
        if($year){
            $year = $year->latest()->paginate(20);
            return $year;
        }else{
            $year = Year::where('year_name', $id)->latest()->paginate(20);
            if($year){
                return $year;
            }else{
                return [];
            }
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'year_name' => ['required', 'string', 'max:191'],
        ]);

        $year = Year::findOrFail($id);
        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $year->update($input);
        return ['message' => 'Updated year info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        
        $Year = Year::findOrFail($id);

        $Year->delete();

        return ['message' => 'Year Deleted'];
    }
}
