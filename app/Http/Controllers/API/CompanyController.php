<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Company;
use App\Models\UserCompany;
use App\Models\Province;
use App\Models\Regency;
use App\Models\District;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $company = Company::where('active', 1);

        if($cari = \Request::get('cariCompany')){
            $company = $company->where(function($query) use ($cari) {
                $query->where('company_name','LIKE', "%$cari%");
            });
        }

        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        $company = $company->orderBy('company_name')->paginate($perPage);

        return $company;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'company_name' => ['required', 'string', 'max:191'],
            'email' => ['nullable', 'email'],
            'address' => ['required', 'string', 'max:191'],
            'classification' => ['required', 'string', 'max:191'],
        ]);

        $input = [
            'company_number' => $request->company_number,
            'company_name' => $request->company_name,
            'website' => $request->website,
            'email' => $request->email,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'phone3' => $request->phone3,
            'active' => 1,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'address' => $request->address,
            'classification' => $request->classification,
            'bank_account_name' => $request->bank_account_name,
            'bank_name' => $request->bank_name,
            'bank_account' => $request->bank_account,
            'insert_by' => Auth::user()->id,
        ];

        if($request->logo){
            array_push($input, ['logo' => $request->logo]);
        }

        return Company::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'company_name' => ['required', 'string', 'max:191'],
            'email' => ['nullable', 'email'],
            'address' => ['required', 'string', 'max:191'],
            'classification' => ['required', 'string', 'max:191'],
        ]);

        $input = [
            'company_name' => $request->company_name,
            'website' => $request->website,
            'email' => $request->email,
            'phone1' => $request->phone1,
            'phone2' => $request->phone2,
            'phone3' => $request->phone3,
            'province' => $request->province,
            'city' => $request->city,
            'district' => $request->district,
            'address' => $request->address,
            'classification' => $request->classification,
            'bank_account_name' => $request->bank_account_name,
            'bank_name' => $request->bank_name,
            'bank_account' => $request->bank_account,
            'update_by' => Auth::user()->id,
        ];

        if($request->logo){
            array_push($input, ['logo' => $request->logo]);
        }

        $company = Company::findOrFail($request->id);
        if( $company ){
            $company->update($input);
            return ['message' => 'Success'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $company = Company::findOrFail($id);
        $company->delete();

        return ['message' => 'Company Deleted'];
    }

    public function province()
    {
        return Province::OrderBy('province_name')->get();
    }

    public function regency()
    {
        if(\Request::get('province_name')){
            $getProv = Province::where('province_name', \Request::get('province_name'))->first();
            return Regency::where('province_id', $getProv->id)->OrderBy('name')->get();
        }else{
            return Regency::where('province_id', 0)->OrderBy('name')->get();
        }
    }

    public function district()
    {
        if(\Request::get('regency_name')){
            $getRegency = Regency::where('name', \Request::get('regency_name'))->first();
            return District::where('regency_id', $getRegency->id)->OrderBy('name')->get();
        }else{
            return District::where('regency_id', 0)->OrderBy('name')->get();
        }
    }

    public function findCompany()
    {
        $companies = [];
        if($search = \Request::get('q')){
            $companies = Company::where(function($query) use ($search){
                $query->where('company_name', 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            return $companies = $this->index();
        }
        return $companies;
    }

    public function findCompanyUser()
    {
        $companies = [];
        if($search = \Request::get('not')){
            $companies = Company::select('uc.user_id', 'companies.id', 'companies.company_name', 'uc.company_id')
            ->leftJoin('user_companies AS uc', function($join) use ($search){
                $join->on('companies.id', '=', 'uc.company_id')
                ->where('uc.active', 1)
                ->where('uc.user_id', '=' , $search);
            })
            ->where(function($query){
                $query->where('companies.active', '1')
                    ->WhereNull('uc.company_id');
            })->get();
        }else if($search = \Request::get('in')){
            $companies = UserCompany::select('user_companies.id', 'user_companies.user_id', 'user_companies.company_id', 
                                            'c.company_name')
            ->leftJoin('companies AS c', function($join){
                $join->on('c.id', '=', 'user_companies.company_id')
                ->where('c.active', 1);
            })
            ->where(function($query) use ($search){
                $query->where('user_companies.active', '1')
                    ->where('user_companies.user_id', '=' , $search);
            })
            ->get();
        }
        return $companies;
    }

    public function storeCompanyUser(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer'],
            'company_id' => ['required', 'integer'],
        ]);

        $input = [
            'user_id' => $request->user_id,
            'company_id' => $request->company_id,
            'active' => 1,
            'insert_by' => Auth::user()->id,
        ];

        return UserCompany::create($input);
    }

    public function updateCompanyUser(Request $request, $id)
    {
        $userCompany = UserCompany::find($id);
        if( $userCompany ){
            $input = $request->all();
            $input['update_by'] = Auth::user()->id;
            $userCompany->update($input);

            return 'Sukses';
        }

        return 'Gagal Update';
    }

    public function deleteCompanyUser($id)
    {
        $userCompany = UserCompany::find($id);
        if( $userCompany ){
            $userCompany->delete();
            return 'Sukses';
        }

        return 'Gagal Delete';
    }
}
