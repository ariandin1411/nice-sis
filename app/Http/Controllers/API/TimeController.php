<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Times;
use Auth;
use DB;

class TimeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $times = Times::where('active', 1);

        $perPage = 10;
        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        if($cari = \Request::get('cariJam')){
            $times = $times->where(function($query) use ($cari) {
                $query->where('time_name','LIKE', "%$cari%")
                ->orWhere('from_time','LIKE', "%$cari%")
                ->orWhere('to_time','LIKE', "%$cari%");
            });
        }

        $times = $times->orderBy('id')->paginate($perPage);
        return $times;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'time_name' => ['required', 'string', 'max:191'],
            'from_time' => ['required', 'string', 'max:191'],
            'to_time' => ['required', 'string', 'max:191'],
        ]);

        $input = $request->all();
        $input['insert_by'] = Auth::user()->id;
        $input['active'] = 1;

        $datetime1 = new \DateTime($request->from_time);
        $datetime2 = new \DateTime($request->to_time);
        $interval = $datetime1->diff($datetime2);
        $intervalString = $interval->format('%H:%i');

        $input['time_interval'] = $intervalString;

        $time = Times::create($input);

        if($time){
            return ["result" => "success" , "result_cd" => "00"];
        }

        return ["result" => "error" , "result_cd" => "01"];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'time_name' => ['required', 'string', 'max:191'],
            'from_time' => ['required', 'string', 'max:191'],
            'to_time' => ['required', 'string', 'max:191'],
        ]);

        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $datetime1 = new \DateTime($request->from_time);
        $datetime2 = new \DateTime($request->to_time);
        $interval = $datetime1->diff($datetime2);
        $intervalString = $interval->format('%H:%i');

        $input['time_interval'] = $intervalString;

        $time = Times::findOrFail($id);
        $time = $time->update($input);

        if($time){
            return ["result" => "success" , "result_cd" => "00"];
        }

        return ["result" => "error" , "result_cd" => "01"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $time = Times::findOrFail($id);
        $time = $time->delete();

        if($time){
            return ["result" => "success" , "result_cd" => "00"];
        }

        return ["result" => "error" , "result_cd" => "01"];
    }
}
