<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Models\Classes;
use App\Models\Schedules;

class ClassesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'showClassesSch']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $classes = Classes::where('active', 1);

        if($cari = \Request::get('cariKelas')){
            $classes = $classes->where(function($query) use ($cari) {
                $query->where('class_name', $cari);
            });
        }

        if($cari = \Request::get('cariKelasSub')){
            $classes = $classes->where(function($query) use ($cari) {
                $query->where('class_sub_name', $cari);
            });
        }

        if($cari = \Request::get('cariKelasMaj')){
            $classes = $classes->where(function($query) use ($cari) {
                $query->where('major', 'Like', "%$cari%");
            });
        }

        if( \Request::get('perPage') ){
            $perPage = \Request::get('perPage');
        }

        $classes = $classes->orderBy('class_name')->orderBy('major')->orderBy('class_sub_name')->paginate($perPage);
        return $classes;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'class_name' => ['nullable'],
            'class_sub_name' => ['nullable'],
            'major' => ['nullable', 'string'],
        ]);

        $input = [
            'class_name' => $request->class_name,
            'class_sub_name' => $request->class_sub_name,
            'major' => $request->major,
            'active' => 1,
            'insert_by' => Auth::user()->id,
            'update_by' => Auth::user()->id,
        ];

        return Classes::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'class_name' => ['nullable'],
            'class_sub_name' => ['nullable'],
            'major' => ['nullable', 'string'],
        ]);

        $input = [
            'class_name' => $request->class_name,
            'class_sub_name' => $request->class_sub_name,
            'major' => $request->major,
            'active' => 1,
            'insert_by' => Auth::user()->id,
            'update_by' => Auth::user()->id,
        ];

        $classes = Classes::findOrFail($request->id);
        if( $classes ){
            $classes->update($input);
            return ['message' => 'Success'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        $classes = Classes::findOrFail($id);
        $classes->delete();

        return ['message' => 'Classes Deleted'];
    }

    public function findClasses()
    {
        $classes = [];
        if($search = \Request::get('q')){
            $classes = Classes::where(function($query) use ($search){
                $query->where('class_name', 'LIKE', "%$search%")
                    ->orWhere('class_sub_name', 'LIKE', "%$search%")
                    ->orWhere('major', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            return $classes = $this->index();
        }
        return $classes;
    }

    public function showClassesSch(Request $request)
    {
        $sch = Schedules::where(['day_name' => $request->day_name, 
            'time' => $request->time, 'semester' => $request->semester])->get();
        $classId = [];
        foreach ($sch as $key => $value) {
            $classId[] = $value->class_id;
        }

        $class = Classes::where(['active' => 1])
                ->whereNotIn('id', $classId)
                ->orderBy('class_name')
                ->orderBy('major')
                ->orderBy('class_sub_name')
                ->paginate(40);

        return $class;
    }
}
