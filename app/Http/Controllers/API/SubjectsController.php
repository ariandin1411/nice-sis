<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Subjects;

class SubjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $subjects = Subjects::orderBy('subject_name');

        if($cari = \Request::get('cariMataPelajaran')){
            $subjects = $subjects->where(function($query) use ($cari) {
                $query->where('subject_name', 'like', "%$cari%");
            });
        }

        if($cari = \Request::get('cariKelas')){
            $subjects = $subjects->where(function($query) use ($cari) {
                $query->where('class_name', $cari);
            });
        }

        if($cari = \Request::get('cariJurusan')){
            $subjects = $subjects->where(function($query) use ($cari) {
                $query->where('major', $cari);
            });
        }

        if( \Request::get('perPage') ){
            $perPage = \Request::get('perPage');
        }

        $subjects = $subjects->paginate($perPage);    

        return $subjects;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject_name' => ['required', 'string', 'max:191'],
        ]);

        return Subjects::create([
            'subject_name' => $request->subject_name,
            'class_name' => $request->class_name,
            'major' => $request->major,
            'active' => 1,
            'insert_by' => Auth::user()->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Subjects::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subject_name' => ['required', 'string', 'max:191'],
        ]);

        $subjects = Subjects::findOrFail($id);
        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $subjects->update($input);
        return ['message' => 'Updated subject info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        
        $subjects = Subjects::findOrFail($id);

        $subjects->delete();

        return ['message' => 'Subjects Deleted'];
    }
}
