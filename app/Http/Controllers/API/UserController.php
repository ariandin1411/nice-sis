<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;
use App\Models\UserClasses;
use App\Models\Classes;
use App\Models\UserParent;
use App\Logs\LogUserClasses;
use App\Models\UserRole;
use App\Models\Schedules;
use Auth;
use DB;
use PDF;
use App\Exports\UsersExport;
use App\Exports\StudentsExport;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'showStudent', 'userParent', 'selectLog', 'showTeachersSch', 'all', 'userPdfView', 'userExcelView', 'studentsExcelView', 'showStudent']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = [];
        if($search = \Request::get('q')){
            $users = User::where(['type' => 'T', 'active' => 1])
                ->where(function($query) use ($search){
                    $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%")
                    // ->orWhere('nis', "$search")
                    ->orWhere('nig', "$search");
            })
            ->latest()
            ->paginate(10);
        }else{
            $users = User::where(['type' => 'T', 'active' => 1])->latest()->paginate(10);
        }
        return $users;
    }

    public function showStudent()
    {
        $users = User::select('users.id', 'users.first_name', 'users.last_name', 'users.nis',
                            'users.gender', 'users.email',
                            'users.address', 'users.phone_number', 'users.year_of_entry', 'kelas.id AS kelas_id',
                            'kelasdet.class_name', 'kelasdet.class_sub_name', 'kelasdet.major',
                            'users.created_at', 'users.birth_date',
                            'users.born_at', 'users.religion', 'users.nisn')
                ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
                // ->Join('user_classes AS kelas', function($join){
                //     $join->on('kelas.user_id', '=', 'users.id')
                //         ->where('kelas.active', 1);
                // })
                ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
                ->where(['users.type' => 'S', 'users.active' => 1]);




        if($cari = \Request::get('cariKelas')){
            if($cari != 0){
                $users = $users->where('kelasdet.id', $cari);
            }
        }else{
            if(!isset($cari) || $cari != 0){
                $users = $users->where('kelas.active', 1);
            }
        }

        if($cari = \Request::get('cariNis')){
                $users = $users->where(function($query) use ($cari){
                            $query->where(DB::raw('CONCAT(users.first_name, " ", users.last_name)'), 'LIKE', "%$cari%")
                                    ->orWhere('users.email', 'LIKE', "%$cari%")
                                    ->orWhere('users.nis', 'LIKE', "%$cari%");
                        });
        }

        $perPage = 10;
        if( $dataPage = \Request::get('perPage') ){
            $perPage = $dataPage;
        }

        return $users->latest()->paginate($perPage);
    }

    public function userParent($user_id)
    {
        $userParent = UserParent::where('user_id', $user_id)->first();
        return $userParent;
    }

    public function userParentUpdate(Request $request, $user_id)
    {
        $userParent = UserParent::where('user_id', $user_id)->first();

        if(!$userParent){

            $userParent = new UserParent();
            $userParent->user_id = $user_id;
            $userParent->insert_by = Auth::user()->id;
            $userParent->active = 1;
        }

        $userParent->parent_mother = $request->parent_mother;
        $userParent->parent_father = $request->parent_father;
        $userParent->phone_number = $request->phone_number;
        $userParent->update_by = Auth::user()->id;

        if($userParent->save()){
            return ['message' => 'Updated user info Success'];
        }
        return ['message' => 'Failed'];
    }

    public function all()
    {
        $users = [];
        if($search = \Request::get('q')){
            $users = User::where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%")
                    ->orWhere('nis', "$search")
                    ->orWhere('nig', "$search");
            })
            ->latest()
            ->paginate(10);
        }else{
            $users = User::where(['active' => 1])->latest()->paginate(10);
        }
        return $users;
        // return User::where(['active' => 1])->latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['nullable', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'nig' => ['nullable', 'string', 'max:191'],
            'nis' => ['nullable', 'string', 'max:191'],
            'gender' => ['required', 'string', 'max:191'],
            'phone_number' => ['required', 'string', 'max:191'],
            'year_of_entry' => ['required', 'date', 'max:191'],
            // 'birth_date' => ['required', 'date', 'max:191'],
            // 'born_at' => ['required', 'string', 'max:191'],
            // 'religion' => ['required', 'string', 'max:191'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            // 'kelas' => ['required'],
        ]);

        // \DB::transaction(function () use ($request, $variable2){
        \DB::transaction(function () use ($request){

            if(!$request->nisn){
                $request->nisn = 0;
            }

            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'nig' => $request->nig,
                'nis' => $request->nis,
                'nisn' => $request->nisn,
                'born_at' => $request->born_at,
                'religion' => $request->religion,
                'birth_date' => $request->birth_date,
                'type' => $request->type,
                'gender' => $request->gender,
                'address' => $request->address,
                'phone_number' => $request->phone_number,
                'year_of_entry' => $request->year_of_entry,
                'birth_date' => $request->birth_date,
                'born_at' => $request->born_at,
                'religion' => $request->religion,
                'active' => 1,
                'email' => $request->email,
                'insert_date' => date('Y-m-d'),
                'insert_by' => Auth::user()->id,
                'password' => Hash::make($request->password)
            ]);

            if($user){
                if($request->type == 'S'){
                    $userRole = new UserRole();
                    $userRole->user_id = $user->id;
                    $userRole->role_id = 4; // this role for siswa
                    $userRole->active = 1;
                    $userRole->insert_by = Auth::user()->id;
                    $userRole->save();
                }
                if($request->kelas_code > 0){
                    $classes = UserClasses::create([
                        'insert_by' => Auth::user()->id,
                        'active' => 1,
                        'class_id' => $request->kelas_code,
                        'user_id' => $user->id
                    ]);

                    if($classes){
                        return $user;
                    }
                }
            }

        });

        return '';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);
        $user = User::select('users.id', 'users.first_name', 'users.last_name', 'users.nis', 'users.gender', 'users.email',
                            'users.address', 'users.phone_number', 'users.year_of_entry', 'kelas.id AS kelas_id',
                            'kelasdet.class_name', 'kelasdet.class_sub_name', 'users.created_at', 'users.birth_date',
                            'users.born_at', 'users.religion', 'users.nisn', 'users.profile_pic', 'kelasdet.major',
                            'users.type', 'users.nig', 'users.religion')
                ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
                ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
                ->where(['users.active' => 1])
                ->where('users.id', $id)
                ->first();

        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $this->validate($request, [
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['nullable', 'string', 'max:191'],
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'nig' => ['nullable', 'string', 'max:191'],
            'nis' => ['nullable', 'string', 'max:191'],
            'gender' => ['required', 'string', 'max:191'],
            'phone_number' => ['required', 'string', 'max:191'],
            'year_of_entry' => ['required', 'date', 'max:191'],
            // 'birth_date' => ['required', 'date', 'max:191'],
            // 'born_at' => ['required', 'string', 'max:191'],
            // 'religion' => ['required', 'string', 'max:191'],
            // 'password' => 'sometimes|string|min:6|confirmed',
        ]);

        // DB::transaction(function () use ($request, $user){
            $input = $request->all();

            if($request->profile_pic && strlen($request->profile_pic) > 20){
                $name = time().'.'.explode('/', explode(':', substr($request->profile_pic, 0, strpos($request->profile_pic, ';')))[1] )[1];
                \Image::make($request->profile_pic)->save(public_path('img/profile/').$name);
                $input['profile_pic'] = $name;

                $oldPhoto = $user->profile_pic;
                $oldPhotoPath = public_path('img/profile/').$oldPhoto;
                if(file_exists($oldPhotoPath)){
                    @unlink($oldPhotoPath);
                }
            }

            $input['password'] = $user->password;
            if($request->password && $request->password != '' && $request->password != null){
                $input['password'] = Hash::make($request->password);
            }

            $input['update_by'] = Auth::user()->id;

            if( $user->update($input) ){
                $count = UserClasses::where('user_id', $user->id)->count();
                if($count == 0){
                    if($request->kelas_code){
                        $classes = new UserClasses ();
                        $classes->insert_by = Auth::user()->id;
                        $classes->active = 1;
                        $classes->class_id = $request->kelas_code;
                        $classes->user_id = $user->id;

                        if($classes->save()){
                            return ['message' => 'Updated user info'];
                        }
                    }

                    return [];

                }else{
                    if($request->kelas_code){
                        $classes = UserClasses::where('user_id', $user->id)->first();
                        $classes->update_by = Auth::user()->id;
                        $classes->class_id = $request->kelas_code;
                        if($classes->save()){
                            return ['message' => "success"];
                        }else{
                            return ['message' => $user->id];
                        }
                    }
                }
            }

        // });
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return ['message' => 'User Deleted'];
    }

    public function search()
    {
        $users = [];
        if($search = \Request::get('q')){
            $users = User::where(['type' => 'T', 'active' => 1])
                ->where(function($query) use ($search){
                    $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                        ->orWhere('email', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            return $users = $this->index();
        }
        return $users;
    }

    public function searchStudent()
    {
        $users = [];
        if($search = \Request::get('q')){
            $users = User::where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                    ->where(['type' => 'S', 'active' => 1])
                    ->orWhere('email', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            return $users = $this->index();
        }
        return $users;
    }

    public function searchAll()
    {
        $users = [];
        if($search = \Request::get('q')){
            $users = User::where(function($query) use ($search){
                $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                    // ->where(['type' => 'T', 'active' => 1])
                    ->orWhere('email', 'LIKE', "%$search%");
            })->paginate(20);
        }else{
            return $users = $this->index();
        }
        return $users;
    }
    public function changeClasses(Request $request)
    {
        // return $request;
        DB::beginTransaction();

        try {
            $i = 0;
            foreach ($request->userIds as $key => $userId) {
                $count = UserClasses::where('user_id', $userId)->count();
                if($count > 0){
                    $userClasses = UserClasses::where('user_id', $userId)->first();
                }else{
                    $userClasses = new UserClasses();
                    $userClasses->user_id = $userId;
                    $userClasses->active = 1;
                    $userClasses->insert_by = Auth::user()->id;
                }

                $activeClasses = Classes::find($request->kelas);

                $userClasses->active = $activeClasses->active;
                $userClasses->class_id = $request->kelas;

                if($userClasses->save()){
                    $i++;
                }else{
                    $i = 0;
                    break;
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }

        if($i > 0){
            return ['message' => 'Success', 'result' => '0'];
        }

        return ['message' => 'Error', 'result' => '1'];
    }

    public function selectLog($user_id)
    {
        $logUserClasses = LogUserClasses::select('log_user_classes.id',
                                                'log_user_classes.user_classes_id AS user_class_id',
                                                'u.first_name', 'u.last_name', 'c.class_name',
                                                'c.class_sub_name', 'c.major',
                                                DB::RAW("concat(u2.first_name, ' ', u2.last_name) AS full_name"),
                                                'log_user_classes.created_at')
                            ->leftJoin('users AS u', 'u.id', '=', 'log_user_classes.user_id')
                            ->leftJoin('classes AS c', 'c.id', '=', 'log_user_classes.to_classes_id')
                            ->leftJoin('users AS u2', 'u2.id', '=', 'log_user_classes.insert_by')
                            ->where('log_user_classes.user_id', $user_id)
                            ->orderBy('log_user_classes.created_at', 'DESC')->get();
        return $logUserClasses;
    }

    public function showHistoryUsers()
    {
        // select a.id,
        // CONCAT_WS(' ', d.class_name, d.major, d.class_sub_name) as student_class,
        // CONCAT_WS(' ', e.class_name, e.major, e.class_sub_name) as teacher_class,
        // f.year_name as student_classes_year_name,
        // g.year_name as teacher_classes_year_name
        // from users a
        // left join user_classes b on a.id = b.user_id
        // left join teacher_classes c on a.id = c.user_id
        // left join classes d on b.class_id = d.id
        // left join classes e on c.class_id = e.id
        // left join years f on b.year_id = f.id
        // left join years g on c.year_id = g.id
        /* left join user_companies h on a.id = h.user_id */
        /* left join companies i on h.company_id = i.id */
        /* left join user_roles j on a.id = j.user_id */
        /* left join roles k on j.role_id = k.id */
        // where a.active = 0
        // and a.type in ('T','S')

        $historyUsers = DB::table('users as a')
                // ->select('a.*',
                // DB::raw("CONCAT_WS(' ', d.class_name, d.major, d.class_sub_name) as student_class, CONCAT_WS(' ', e.class_name, e.major, e.class_sub_name as teacher_class"), 'f.year_name as student_classes_year_name', 'g.year_name as teacher_classes_year_name')
                ->select('a.*', DB::raw('CONCAT_WS(" ", d.class_name, d.major, d.class_sub_name) AS student_class, CONCAT_WS(" ", e.class_name, e.major, e.class_sub_name) AS teacher_class'),'f.year_name as student_classes_year_name','g.year_name as teacher_classes_year_name')
                ->leftJoin('user_classes as b', 'a.id', '=', 'b.user_id')
                ->leftJoin('teacher_classes as c', 'a.id', '=', 'c.user_id')
                ->leftJoin('classes as d', 'b.class_id', '=', 'd.id')
                ->leftJoin('classes as e', 'c.class_id', '=', 'e.id')
                ->leftJoin('years as f', 'b.year_id', '=', 'f.id')
                ->leftJoin('years as g', 'c.year_id', '=', 'g.id')
                ->where(['a.active' => 0]);
                // ->latest()
                // ->paginate(20);

                if($cari = \Request::get('cari-kelas')){
                    if($cari != 0){
                        $historyUsers = $historyUsers->where(function($query) use($cari) {
                            $query->where('d.id', $cari)->orWhere('e.id', $cari);
                        });
                    }
                }

        $historyUsers = $historyUsers->latest()->paginate(40);

        return $historyUsers;
    }

    public function showTeachersSch(Request $request)
    {
        $sch = Schedules::where(['day_name' => $request->day_name,
            'time' => $request->time, 'semester' => $request->semester])->get();
        $uIds = [];
        foreach ($sch as $key => $value) {
            $uIds[] = $value->user_id;
        }

        $user = User::where(['type' => 'T', 'active' => 1])
                ->whereNotIn('id', $uIds)
                ->orderBy('first_name')->paginate(40);

        return $user;
    }

    public function userPdfView(){
        $teacherData = User::where(['type' => 'T', 'active' => 1])->latest()->get();
        $data = ['title' => 'SMA N 2 Cibinong', 'teacherData' => $teacherData];
        $pdf = PDF::loadView('users.user-pdf-view', $data)
                ->setPaper('a4', 'landscape');
                // ->setPaper('a4', 'potrait');
        return $pdf->stream('user-pdf-view.pdf',array('Attachment'=>1));
    }

    public function userExcelView(){
        return Excel::download(new UsersExport($pdam_code, $date), 'teacher-list.xlsx');
    }

    public function studentsExcelView(){
        return Excel::download(new StudentsExport, 'students-list.xlsx');
    }
}
