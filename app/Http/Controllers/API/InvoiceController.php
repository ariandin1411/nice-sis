<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoices;
use App\Models\InvoiceDets;
use App\User;
use App\Models\Year;
use Auth;
use DB;
use Illuminate\Support\Facades\Log;

class InvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show', 'destroy']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Log::info('[InvoiceController : Masuk ke InvoiceController function index]');
        $thnAjaran = '2019 / 2020';
        $datemonth = substr($thnAjaran, 0, 4);
        $datemonth2 = substr($thnAjaran, -4);

        $invoiceUsers = [];
        if($request->year_id && $request->class_id){

            $yearInfo = Year::find($request->year_id);
            $thnAjaran = $yearInfo->year_name;
            $datemonth = substr($thnAjaran, 0, 4);
            $datemonth2 = substr($thnAjaran, -4);


            $invoiceUsers = User::select(
                                        'users.id',
                                        DB::RAW('concat(users.first_name, " ", users.last_name) AS full_name'),
                                        'users.nis',
                                        DB::RAW('concat(c.class_name, c.major, " - ", c.class_sub_name) AS user_class_name'),
                                        'i.id AS invoice_id',
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-07",concat(i.total_amt,"_",i.id),null))) as jul'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-08",concat(i.total_amt,"_",i.id),null))) as agu'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-09",concat(i.total_amt,"_",i.id),null))) as sep'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-10",concat(i.total_amt,"_",i.id),null))) as okt'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-11",concat(i.total_amt,"_",i.id),null))) as nov'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth.'-12",concat(i.total_amt,"_",i.id),null))) as des'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-01",concat(i.total_amt,"_",i.id),null))) as jan'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-02",concat(i.total_amt,"_",i.id),null))) as feb'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-03",concat(i.total_amt,"_",i.id),null))) as mar'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-04",concat(i.total_amt,"_",i.id),null))) as apr'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-05",concat(i.total_amt,"_",i.id),null))) as mei'),
                                        DB::RAW('group_concat((if(DATE_FORMAT(i.from_dt,"%Y-%m") = "'.$datemonth2.'-06",concat(i.total_amt,"_",i.id),null))) as jni')
                                        )
                        ->leftJoin('invoices AS i', function($join) use($request){
                            $join->on('users.id', '=', 'i.user_id')->where('thn_ajaran', $request->year_id);
                        })
                        ->leftJoin('user_classes AS uc', function($join){
                            $join->on('users.id', '=', 'uc.user_id');
                        })
                        ->leftJoin('classes AS c', function($join){
                            $join->on('uc.class_id', '=', 'c.id');
                        })
                        ->where('users.active', 1)
                        ->where('users.type', 'S')
                        ->where('c.id', $request->class_id)
                        ->orderBy(DB::RAW('concat(users.first_name, " ", users.last_name)'))
                        ->groupBy('users.id')
                        ->get();
        }
        return $invoiceUsers;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'invoice_no' => ['required'],
            'user_id' => ['required', 'integer'],
            'total_amt' => ['required', 'between:0,99.99'],
            // 'thn_ajaran' => ['required', 'integer'],
            // 'from_dt' => ['required', 'date'],
            // 'to_dt' => ['required', 'date']
        ]);

        if($request->month && is_array($request->month)){

            DB::beginTransaction();

            try {
                foreach ($request->month as $key => $value) {
                    if(intval($value) > 6){
                        $thnAjaran = $request->year.' / '.($request->year+1);
                    }else{
                        $thnAjaran = ($request->year-1).' / '.$request->year;
                    }

                    $year = Year::where('year_name', $thnAjaran)->first();
                    if(!$year){
                        $year = new Year();
                        $year->year_name = $thnAjaran;
                        $year->active = 0;
                        $year->insert_by = $request->user_id;
                        $year->save();
                    }

                    $insert = [
                        'from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$request->year)),
                        'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$request->year)),
                        'invoice_no' => $this->selMaxInv(),
                        'user_id' => $request->user_id,
                        'thn_ajaran' => $year->id,
                        'total_amt' => $request->total_amt,
                        'insert_by' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ];
                    
                    $queryPrep = Invoices::where(['from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$request->year)),
                                                'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$request->year)), 
                                                'user_id' => $request->user_id
                                                ]);

                    $invCheck = $queryPrep->count();

                    if($invCheck <= 0){
                        $invInput = DB::table('invoices')->insert($insert);
                        $invSel = $queryPrep->first();
                        if($invSel) {
                            if( $request->prod_id && is_array($request->prod_id) ){
                                $invDetRows = [];
                                foreach ($request->prod_id as $key2 => $value2) {
                                    $invDetRows[] = [
                                        'invoice_id' => $invSel->id,
                                        'prod_id' => $value2,
                                        'prod_nm' => $request->prod_nm[$key2],
                                        'sale_prc' => $request->sale_prc[$key2],
                                        'insert_by' => Auth::user()->id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ];
                                }
                                $invInputDets = DB::table('invoice_dets')->insert($invDetRows);
                            }
                        }
                    }
                }

                DB::commit();
                return ['result' => 'Success', 'code' => '00'];
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                return ['result' => $e, 'code' => '01'];
            }
        }

        // $input = $request->all();
        // $input['insert_by'] = Auth::user()->id;

        // $invoice = Invoices::create($input);

        // if($invoice){
        //     return ['result' => 'Success', 'code' => '00'];
        // }

        // return ['result' => 'Error', 'code' => '01'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if($user){
            return $user;
        }

        return [];
    }

    public function showInvoice($id)
    {
        $invoice = Invoices::find($id);
        if($invoice){
            $invoiceDets = InvoiceDets::where('invoice_id', $id)->get();
            if($invoiceDets && count($invoiceDets) > 0 ){
                // return ['invoice' => $invoice, 'invoiceDets' => $invoiceDets];
                return ['invoice' => $invoice, 'invoiceDets' => $invoiceDets, 'invoiceYear' => $invoice->years];
            }
            return ['invoice' => $invoice, 'invoiceDets' => []];
        }
        return ['invoice' => [], 'invoiceDets' => []];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'invoice_no' => ['required'],
            'user_id' => ['required', 'integer'],
            'total_amt' => ['required', 'between:0,99.99'],
            'thn_ajaran' => ['required', 'integer'],
            'from_dt' => ['required', 'date'],
            'to_dt' => ['required', 'date']
        ]);

        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $invoice = Invoices::find($id);

        if($invoice){

            $invoice = $invoice->update($input);

            if($invoice){
                return ['result' => 'Success', 'code' => '00'];
            }
        }

        return ['result' => 'Error', 'code' => '01'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoice = Invoices::find($id)->delete();

        if($invoice){
            return ['result' => 'Success', 'code' => '00'];
        }

        return ['result' => 'Error', 'code' => '01'];
    }

    private function selMaxInv()
    {
        $inv = 0;
        $maxInv = Invoices::where(DB::RAW('SUBSTR(invoice_no, 5,6)'), date('ymd'))
                ->max('invoice_no');
                // ->toSql();
        // dd($maxInv);
        if(!$maxInv){
            $inv = 'INV_'.date('ymd').str_pad("1",4,"0", STR_PAD_LEFT);
        }else{
            $inv = substr($maxInv, 4);
            $inv = $inv+1;
            $inv = 'INV_'.$inv;
        }
        return $inv;
    }
}
