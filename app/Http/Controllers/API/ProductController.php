<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Products;
use Auth;
use DB;

class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productList = Products::where('active', 1);

        $perPage = 10;
        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        if($cari = \Request::get('cariProduct')){
            $productList = $productList->where(function($query) use ($cari) {
                $query->where('product_name','LIKE', "%$cari%");
            });
        }

        $productList = $productList->orderBy('product_name')->paginate($perPage);

        return $productList;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

 

    public function store(Request $request)
    {
        $this->validate($request, [
            'product_no' => ['required', 'string', 'max:191'],
            'product_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required', 'integer'],
            'sale_price' => ['required', 'numeric'], 
        ]);

        $product = new Products();
        $product->product_no = $request->product_no; 
        $product->product_name = $request->product_name;
        $product->company_id = $request->company_id;
        $product->sale_price = $request->sale_price;
        $product->insert_by = Auth::user()->id;
        $product->active = 1; 

        $product->save();

        return ["result" => "success" , "result_cd" => "00"];


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'product_no' => ['required', 'string', 'max:191'],
            'product_name' => ['required', 'string', 'max:191'],
            'company_id' => ['required', 'integer'],
            'sale_price' => ['required', 'numeric'], 
        ]);

        $product = Products::find($id);

        $input = $request->all();

        $input['update_by'] = Auth::user()->id;
        if($product){
            $product = $product->update($input);
        }

        if($product){
            return ["result" => "success" , "result_cd" => "00"];    
        }else{
            return ["result" => "error" , "result_cd" => "01"];
        }

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Products::findOrFail($id);

        $product->delete();

        return ["result" => "success" , "result_cd" => "00"];    
    }
}
