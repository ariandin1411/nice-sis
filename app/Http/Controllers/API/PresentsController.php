<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserClasses;
use App\Models\UserParent;
use App\Logs\LogUserClasses;
use App\Models\UserRole;
use App\Models\Schedules;
use App\Models\Presents;
use Auth;
use DB;
use App\Lib\NicepayDirect\NicepayLib;

class PresentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'presentsList']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('users.id', 'users.first_name', 'users.last_name', 'users.nis', 'users.gender', 'users.email',
                            'users.address', 'users.phone_number', 'users.year_of_entry', 'kelas.id AS kelas_id',
                            'kelasdet.class_name', 'kelasdet.class_sub_name', 'kelasdet.major',
                            'users.created_at', 'users.birth_date', 'p.semester', 'p.year_id', 'y.year_name', 'p.date_in',
                            'users.born_at', 'users.religion', 'users.nisn', 'p.id AS presents_id', 'p.desc')
                ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
                ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
                // ->leftJoin('presents AS p', 'p.user_id', '=', 'users.id')
                // ->leftJoin('years AS y', 'y.id', '=', 'p.year_id')
                ->where(['users.type' => 'S', 'users.active' => 1]);

        if($cari = \Request::get('cariKelas')){
            if($cari != 0){
                $users = $users->where('kelasdet.id', $cari);
            }
        }

        if($cari = \Request::get('cariSubjct')){
            if($cari != 0){
                $users = $users->where('s.subject_id', $cari);
            }
        }

        if($cari = \Request::get('cariSemester')){
            if($cari != 0){
                $users = $users->where('p.semester', $cari);
            }
        }

        if($cari = \Request::get('cariTanggal')){
            if($cari != 0){
                $users = $users->leftJoin('presents AS p', function ($join) use ($cari) {
                        $join->on('users.id', '=', 'p.user_id')->where('p.date_in', $cari);
                    })
                    ->leftJoin('years AS y', 'y.id', '=', 'p.year_id');
            }
        }else{
            if($sDate == null){
                $sDate = date('Y-m-d');
                $users = $users->leftJoin('presents AS p', function ($join) use ($sDate) {
                    $join->on('users.id', '=', 'p.user_id')->where('p.date_in', $sDate);
                })
                ->leftJoin('years AS y', 'y.id', '=', 'p.year_id');
            }else{
                $users = $users->leftJoin('presents AS p', function ($join) use ($sDate) {
                    $join->on('users.id', '=', 'p.user_id')->where('p.date_in', $sDate);
                })
                ->leftJoin('years AS y', 'y.id', '=', 'p.year_id');
            }
        }

        if($cari = \Request::get('cariTahun')){
            $users = $users->where('y.id', $cari);
        }

        if($cari = \Request::get('cariNis')){
                $users = $users->where(function($query) use ($cari){
                            $query->where(DB::raw('CONCAT(users.first_name, " ", users.last_name)'), 'LIKE', "%$cari%")
                                    ->orWhere('users.email', 'LIKE', "%$cari%")
                                    ->orWhere('users.nis', 'LIKE', "%$cari%");
                        });
        }

        return $users->latest()->paginate(40);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            $input = $request->all();
            $input['active'] = 1;
            $input['insert_by'] = Auth::user()->id;

            $rowInsert = [];

            if( $request->student && is_array($request->student) ){
                foreach ($request->student as $key => $student) {
                    $sqlCek = Presents::where(['date_in' => $request->date_in, 'user_id' => $student]);
                    $cekData = $sqlCek->count();

                    if($cekData == 0){
                        $rowInsert[] = [
                            'user_id' => $student, 
                            'desc' => $request->present[$key], 
                            'year_id' => $request->year, 
                            'semester' => $request->semester, 
                            'active' => 1, 
                            'insert_by' => Auth::User()->id,
                            'date_in' => $request->date_in,
                            'created_at' => date('Y-m-d H:i:s')
                        ];
                    }else{
                        $sqlCek = $sqlCek->first();
                        $sqlCek->desc = $request->present[$key];
                        $sqlCek->updated_at = date('Y-m-d H:i:s');
                        $sqlCek->update_by = Auth::User()->id;
                        $sqlCek->save();
                    }
                }
                $present = Presents::insert($rowInsert);
            }
            DB::commit();
            return ['result' => 'Success', 'code' => '00'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function presentsList(Request $request)
    {
        $dateReq = $request->dateReq;
        $dateReq2 = date('Y-m', strtotime($request->dateReq));
        $date = date('t', strtotime($dateReq2));

        $presentsList = $this->index();

        $mainData = [];
        foreach ($presentsList as $key => $value) {
            // echo 'nis : '.$value->class_name.' '.$value->class_sub_name.' - '.$value->desc.' '.$value->last_name.'<br />';
            $tanggal = 1;
            $mainData[$key]['nis'] = $value->nis;
            $mainData[$key]['id'] = $value->id;
            $mainData[$key]['first_name'] = $value->first_name;
            $mainData[$key]['last_name'] = $value->last_name;
            $mainData[$key]['class_name'] = $value->class_name;
            $mainData[$key]['class_sub_name'] = $value->class_sub_name;
            $mainData[$key]['major'] = $value->major;
            for ($i=0; $i < $date; $i++) { 
                $mainData[$key]['dateList'][$i]['dateList'] = date('d', strtotime($dateReq2.'-'.$tanggal));
                $mainData[$key]['dateList'][$i]['fullDateList'] = date('Y-m-d', strtotime($dateReq2.'-'.$tanggal));

                $selPresent = Presents::where(['user_id' => $value->id, 
                                                'date_in' => $mainData[$key]['dateList'][$i]['fullDateList']])->first();

                $mainData[$key]['dateList'][$i]['studentList'] = $value->id;
                if( $selPresent ){
                    $mainData[$key]['dateList'][$i]['presentsList'] = $selPresent->desc;
                }else{
                    $mainData[$key]['dateList'][$i]['presentsList'] = '-';
                }

                $tanggal++;
            }
        }

        // print_r($mainData);
        // die;

        return ['dateList' => $mainData, 'dateName' => date('F Y', strtotime($request->dateReq)), 'dayTotal' => $date];
        // for($i=0; $i < date('d', strtotime()))
    }
}
