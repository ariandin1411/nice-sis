<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Schedules;
use App\Models\UserParent;
use App\Logs\LogUserClasses;
use App\Models\UserRole;
use Auth;
use DB;

class ScheduleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $schedule = [];
        if($request->class_id && $request->semester && $request->year_id){
            $schedule = Schedules::select(
                                        'schedules.id',
                                        't.time_name', 
                                        't.from_time',
                                        's.subject_name' ,
                                        'schedules.subject',
                                        'schedules.user_id',
                                        'schedules.time',
                                        'schedules.class_id',
                                        'schedules.day_name',
                                        'schedules.semester',
                                        'schedules.schedule_type',
                                        'schedules.year_id', 
                                        'y.year_name', 
                                        DB::RAW('concat(c.class_name, " ", c.major, " - ", c.class_sub_name) AS class_name'),
                                        DB::RAW('concat(u.first_name, " ", u.last_name) AS user_name'),
                                        DB::RAW('group_concat(if(schedules.day_name = "senin", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS senin'),
                                        DB::RAW('group_concat(if(schedules.day_name = "selasa", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS selasa'),
                                        DB::RAW('group_concat(if(schedules.day_name = "rabu", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS rabu'),
                                        DB::RAW('group_concat(if(schedules.day_name = "kamis", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS kamis'),
                                        DB::RAW('group_concat(if(schedules.day_name = "jumat", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS jumat'),
                                        DB::RAW('group_concat(if(schedules.day_name = "sabtu", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS sabtu'),
                                        DB::RAW('group_concat(if(schedules.day_name = "minggu", concat_ws(" : ", concat(u.first_name, " ", u.last_name), s.subject_name), null)) AS minggu')
                                     )
                    ->leftJoin('times AS t', 't.id', '=', 'schedules.time')
                    ->leftJoin('users AS u', 'u.id', '=', 'schedules.user_id')
                    ->leftJoin('subjects AS s', 's.id', '=', 'schedules.subject')
                    ->leftJoin('classes AS c', 'c.id', '=', 'schedules.class_id')
                    ->leftJoin('years AS y', 'y.id', '=', 'schedules.year_id')
                    ->where(['schedules.active' => 1, 'schedules.class_id' => $request->class_id])
                    ->where('schedules.year_id', $request->year_id)
                    ->where('schedules.semester', $request->semester)
                    ->groupBy('t.time_name',  
                                'schedules.schedule_type', 'schedules.year_id')
                    ->paginate(20);
        }
        
        return $schedule;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'subject' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'time' => ['required'],
            'day_name' => ['required', 'string', 'max:191'],
            'schedule_type' => ['required', 'string', 'max:191'],
            'year_id' => ['required', 'integer'],
            'semester' => ['required', 'integer', 'max:191'],
            'class_id' => ['required', 'integer'],
        ]);

        \DB::transaction(function () use ($request){

            $input = $request->all();
            $input['active'] = 1;
            $input['insert_by'] = Auth::user()->id;

            $schedule = Schedules::create($input);

            return ['result' => 'Success', 'code' => '00'];
        });

        return ['result' => 'Error', 'code' => '01'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $schedule = Schedules::select(
                                    'schedules.id',
                                    't.time_name', 
                                    't.from_time',
                                    's.subject_name' ,
                                    'schedules.subject',
                                    'schedules.user_id',
                                    'schedules.time',
                                    'schedules.class_id',
                                    'schedules.day_name',
                                    'schedules.semester',
                                    'schedules.schedule_type',
                                    'schedules.year_id', 
                                    'y.year_name', 
                                    DB::RAW('concat(c.class_name, " ", c.major, " - ", c.class_sub_name) AS class_name'),
                                    DB::RAW('concat(u.first_name, " ", u.last_name) AS user_name'),
                                    'schedules.day_name'
                                 )
                ->leftJoin('times AS t', 't.id', '=', 'schedules.time')
                ->leftJoin('users AS u', 'u.id', '=', 'schedules.user_id')
                ->leftJoin('subjects AS s', 's.id', '=', 'schedules.subject')
                ->leftJoin('classes AS c', 'c.id', '=', 'schedules.class_id')
                ->leftJoin('years AS y', 'y.id', '=', 'schedules.year_id')
                ->where(['schedules.active' => 1]);

                if( $request->class_id ) {
                    $schedule = $schedule->where('schedules.class_id', $request->class_id);
                }

                if( $request->year_id ) {
                    $schedule = $schedule->where('schedules.year_id', $request->year_id);
                }

                if( $request->semester ) {
                    $schedule = $schedule->where('schedules.semester', $request->semester);   
                }
                
                $schedule = $schedule->paginate(20);

        return $schedule;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'subject' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'time' => ['required'],
            'day_name' => ['required', 'string', 'max:191'],
            'schedule_type' => ['required', 'string', 'max:191'],
            'year_id' => ['required', 'integer'],
            'semester' => ['required', 'integer', 'max:191'],
            'class_id' => ['required', 'integer'],
        ]);

        \DB::transaction(function () use ($request, $id){

            $schedule = Schedules::find($id);

            if($schedule){
                $input = $request->all();
                $input['update_by'] = Auth::user()->id;

                $schedule = $schedule->update($input);

                return ['result' => 'Success', 'code' => '00'];
            }
        });

        return ['result' => 'Error', 'code' => '01'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedule = Schedules::findOrFail($id);

        $schedule = $schedule->delete();

        if($schedule){
            return ["result" => "success" , "result_cd" => "00"];
        }

        return ["result" => "error" , "result_cd" => "01"];
    }
}
