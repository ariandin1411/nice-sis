<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\TeacherClass;
use App\User;
use App\Models\Classes;
use App\Models\Year;
use DB;
use App\Exports\TeacherClassesExport;
use Maatwebsite\Excel\Facades\Excel;

class TeacherClassesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['teacherClassesExcelView', 'index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teacherClass = DB::table('teacher_classes AS a')
                ->select('a.*', 'b.first_name', 'b.last_name', 'c.class_name', 'c.class_sub_name', 'd.year_name', 
                        'c.major', 'b.nig')
                ->leftJoin('users AS b', 'a.user_id', '=', 'b.id')
                ->leftJoin('classes AS c', 'a.class_id', '=', 'c.id')
                ->leftJoin('years AS d', 'a.year_id', '=', 'd.id')
                ->where(['a.active' => 1, 'b.active' => 1, 'c.active' => 1, 'd.active' => 1, 'b.type' => 'T']);
        
        if($cariKelas = \Request::get('cariKelas')){
            $teacherClass = $teacherClass->where('a.class_id', $cariKelas);
        }

        if($cariGuru = \Request::get('cariGuru')){
            $teacherClass = $teacherClass->where(function($query) use($cariGuru){
                $query->where(DB::raw('CONCAT(b.first_name, " ", b.last_name)'), 'LIKE', "%$cariGuru%")
                                    ->orWhere('b.email', 'LIKE', "%$cariGuru%")
                                    ->orWhere('b.nig', '=', "$cariGuru");
            });
        }

        $teacherClass = $teacherClass->latest()->paginate(20);

        return $teacherClass;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'max:191'],
            'class_id' => ['required', 'integer', 'max:191'],
            'year_id' => ['required', 'integer', 'max:191'],
        ]);

        return TeacherClass::create([
            'user_id' => $request->user_id,
            'class_id' => $request->class_id,
            'year_id' => $request->year_id,
            'active' => 1,
            'insert_by' => Auth::user()->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'max:191'],
            'class_id' => ['required', 'integer', 'max:191'],
            'year_id' => ['required', 'integer', 'max:191'],
        ]);

        $teacherClass = TeacherClass::findOrFail($id);
        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $teacherClass->update($input);
        return ['message' => 'Updated teacher class info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        
        $teacherClass = TeacherClass::findOrFail($id);

        $teacherClass->delete();

        return ['message' => 'Teacher Class Deleted'];
    }

    public function teacherClassesExcelView(){
        return Excel::download(new TeacherClassesExport, 'teacherclasses-list.xlsx');
    }
}
