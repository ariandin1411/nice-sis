<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\UserClasses;
use App\Models\UserParent;
use App\Logs\LogUserClasses;
use App\Models\UserRole;
use App\Models\Schedules;
use App\Models\Subjects;
use App\Models\Scores;
use App\Models\Classes;
use Auth;
use DB;

class ScoreController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['index']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params = null;
        $perPage = 10;

        $users = User::select('users.id', 'users.first_name', 'users.last_name', 'users.nis', 'users.gender', 
                            'users.email',
                            'users.address', 'users.phone_number', 'users.year_of_entry', 'kelas.id AS kelas_id',
                            'kelasdet.class_name', 'kelasdet.class_sub_name', 'kelasdet.major',
                            'users.created_at', 'users.birth_date', 's.semester', 's.year_id', 'y.year_name',
                            'users.born_at', 'users.religion', 'users.nisn', 
                            'u2.id AS teacher_id',
                            DB::RAW('concat(u2.first_name," ", u2.last_name) AS teacher_name'),
                            DB::RAW('if(s.score is null, 0, s.score) AS score'),
                            DB::RAW('group_concat(s2.subject_name) AS subject_name'), 
                            DB::RAW('group_concat(s2.id) AS subject_id'),
                            DB::RAW('group_concat(s.id) AS score_id'))
                ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
                ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
                ->leftJoin('scores AS s', 's.user_id', '=', 'users.id')
                ->leftJoin('users AS u2', 'u2.id', '=', 's.teacher_id')
                ->leftJoin('subjects AS s2', 's2.id', '=', 's.subject_id')
                // ->leftJoin('years AS y', 'y.id', '=', 's.year_id')
                ->where(['users.type' => 'S', 'users.active' => 1]);

        if($cari = \Request::get('cariKelas')){
            if($cari != 0){
                $users = $users->where('kelasdet.id', $cari);

                $classesList = $this->classesList($cari);
                $params = [
                            'class_name' => $classesList->class_name,
                            'major' => $classesList->major
                          ];
            }
        }

        if($cari = \Request::get('cariSubjct')){
            if($cari != 0){
                $users = $users->where('s.subject_id', $cari);
            }
        }

        if($cari = \Request::get('cariSemester')){
            if($cari != 0){
                $users = $users->where('s.semester', $cari);
            }
        }

        if($cari = \Request::get('cariNis')){
                $users = $users->where(function($query) use ($cari){
                            $query->where(DB::raw('CONCAT(users.first_name, " ", users.last_name)'), 'LIKE', "%$cari%")
                                    ->orWhere('users.email', 'LIKE', "%$cari%")
                                    ->orWhere('users.nis', 'LIKE', "%$cari%");
                        });
        }

        if($cari = \Request::get('cariThnAjaran')){
                $users = $users->join('years AS y', function($join) use ($cari){
                            $join->on('y.id', '=', 's.year_id')
                            ->where('s.year_id', $cari);
                        });
        }else{
                $users = $users->leftJoin('years AS y', function($join) use ($cari){
                            $join->on('y.id', '=', 's.year_id');
                        });
        }

        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        $users = $users->orderBy('users.first_name')
                ->orderBy('users.last_name')
                ->groupBy('users.id')
                ->paginate($perPage);

        $subjectList = $this->subjectList($params);

        $subjectName = [];
        $subjectId = [];
        foreach ($subjectList as $k => $v) {
            $subjectName[] = $v->subject_name;
            $subjectId[] = $v->id;
        }

        $scoreList = [];
        foreach ($users as $key => $value) {
            $value->subjectsList = $subjectName;
            $cek = $this->show($value->score_id);
            
            if( count($subjectId) > 0 ){
                foreach ($subjectId as $ksi => $si) {
                    if(count($cek) > 0){
                        $score = 0;
                        foreach ($cek as $kc => $vc) {
                            if($si == $vc->subject_id){
                                $score = $vc->score;
                                break;
                            }
                        }
                        $scoreList[$key][$ksi] = $score;
                    }else{
                        $scoreList[$key][$ksi] = 0;
                    }
                }
            }else{
                $scoreList[$key][] = 0;
            }
            $value->scoreList = $scoreList[$key];
        }

        return ['users' => $users, 'subjects' => $subjectName];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();

        try {
            // $input = $request->all();
            $input['user_id'] = $request->user_id;
            $input['subject_id'] = $request->subject_id;
            $input['teacher_id'] = $request->teacher_id;
            $input['score'] = $request->score;
            $input['active'] = 1;
            $input['year_id'] = $request->year_id;
            $input['semester'] = $request->semester;
            $input['insert_by'] = Auth::user()->id;

            $count = Scores::where(['user_id' => $request->user_id, 'subject_id' => $request->subject_id, 
                                    'teacher_id' => $request->teacher_id])->count();

            if($count <= 0){
                Scores::create($input);
                DB::commit();
            }
            return ['result' => $input, 'code' => '00'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // if(is_array($id)){
            $score = Scores::whereIn('id', explode(',', $id))->get();
        // }else{
        //     $score = Scores::find($id);
        // }
        return $score;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer'],
            'subject_id' => ['required', 'integer'],
            'teacher_id' => ['required', 'integer'],
            'score' => ['required', 'between:0,99.99'],
            'year_id' => ['required', 'integer'],
            'semester' => ['required', 'integer'],
        ]);

        $score = Scores::find($id);

        if($id == null || $request->score_id == null){
            return $this->store($request);
        }

        if($score->subject_id != $request->subject_id){
            return $this->store($request);
        }

        DB::beginTransaction();

        try {
            $input = $request->all();
            $input['update_by'] = Auth::user()->id;
            
            $score->update($input);
            DB::commit();
            return ['result' => 'Success', 'code' => '00'];
        } catch (\Exception $e) {
            DB::rollback();
            return ['result' => $e, 'code' => '01'];
        }

        return $request;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function classesList($id)
    {
        $classes = Classes::find($id);
        return $classes;
    }

    private function subjectList($parmas = null)
    {
        $subjects = [];
        if($parmas != null){
            $subjects = Subjects::where('active', 1);

            if( isset($parmas['class_name']) ){
                $subjects = $subjects->where('class_name', $parmas['class_name']);
            }

            if( isset($parmas['major']) ){
                $subjects = $subjects->where('major', $parmas['major']);
            }

            $subjects = $subjects->get();
        }
        return $subjects;
    }
}
