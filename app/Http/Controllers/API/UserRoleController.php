<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserRole;
use Auth;
use DB;

class UserRoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $perPage = 10;
        $userLore = UserRole::select('u.first_name', 'u.last_name', 'user_roles.id', 'u.id AS user_id', 'r.id AS role_id', 'r.role_name', 'user_roles.created_at')
                ->join('users AS u', 'u.id', '=', 'user_roles.user_id')
                ->join('roles AS r', 'r.id', '=', 'user_roles.role_id');

        if($cari = \Request::get('cariNis')){
            $userLore = $userLore->where(function($query) use ($cari) {
                $query->where('r.role_name','LIKE', "%$cari%")
                        ->orWhere(DB::RAW('concat(u.first_name," ", u.last_name)'),'LIKE', "%$cari%");
            });
            // return $userLore->toSql();
        }

        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        $userLore = $userLore->orderBy('u.first_name')->paginate($perPage);

        return $userLore;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'max:191'],
            'role_id' => ['required', 'integer', 'max:191'],
        ]);

        return UserRole::create([
            'user_id' => $request->user_id,
            'role_id' => $request->role_id,
            'active' => 1,
            'insert_by' => Auth::user()->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => ['required', 'integer', 'max:191'],
            'role_id' => ['required', 'integer', 'max:191'],
        ]);

        $role = UserRole::findOrFail($id);
        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $role->update($input);
        return ['message' => 'Updated user role info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        
        $Role = UserRole::findOrFail($id);

        $Role->delete();

        return ['message' => 'Role Deleted'];
    }

    public function rolePerUser($userId)
    {
        $select = UserRole::select('u.first_name', 'u.last_name', 'user_roles.id', 
                'u.id AS user_id', 'r.id AS role_id', 'r.role_name', 'user_roles.created_at')
                ->join('users AS u', 'u.id', '=', 'user_roles.user_id')
                ->join('roles AS r', 'r.id', '=', 'user_roles.role_id')
                ->where('u.id', $userId)
                ->orderBy('r.role_name', 'ASC')
                ->get();

        return $select;
    }
}
