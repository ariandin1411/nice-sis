<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Invoices;
use App\Models\InvoiceDets;
use App\Models\Payment;
use App\Models\PaymentDet;
use App\Models\Year;
use App\User;
use Auth;
use DB;
use App\Lib\NicepayDirect\NicepayLib;
use App\Lib\NicepayRedirect\NicepayLibRedirect;
use App\Mail\VaCreated;
use App\Mail\VaSuccess;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['noti']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = DB::table('invoices as a')
                ->select('a.id','a.invoice_no', 'a.user_id',
                        DB::raw('CONCAT_WS(" ", c.first_name, c.last_name) AS user_name'),
                        'a.total_amt', 'b.year_name', 'a.from_dt', 'a.to_dt', 'd.bank_cd',
                        'd.pay_method', 'd.txid', 'd.vacct_no')
                ->leftJoin('years as b', 'a.thn_ajaran', '=', 'b.id')
                ->leftJoin('users as c', 'a.user_id', '=', 'c.id')
                ->leftJoin('payments as d', 'a.invoice_no', '=', 'd.payment_no')
                ->where(['a.user_id'=> Auth::user()->id, 'c.type' => 'S'])
                // ->where(['a.user_id'=> 24, 'c.type' => 'S'])
                ->orderBy('a.from_dt', 'desc')
                ->get();

        return $payments;
    }

    public function paymentPg()
    {
        $payments = DB::table('payments as a')
                ->select('a.id','a.payment_no', 'a.user_id',
                        DB::raw('CONCAT_WS(" ", c.first_name, c.last_name) AS user_name'),
                        'a.total_amt', 'b.year_name', 'a.from_dt', 'a.to_dt',
                        'a.status', 'a.pay_method', 'a.txid', 'a.vacct_no', 'a.bank_cd')
                ->leftJoin('years as b', 'a.thn_ajaran', '=', 'b.id')
                ->leftJoin('users as c', 'a.user_id', '=', 'c.id')
                ->where(['a.user_id'=> Auth::user()->id, 'c.type' => 'S'])
                // ->where(['a.user_id'=> 24, 'c.type' => 'S'])
                ->orderBy('a.from_dt', 'desc')
                ->get();

        $inv = $this->index();

        $paymentDatas = [];
        $cekDate = [];
        $i = 0;
        foreach ($inv as $key => $value) {
            $paymentDatas[$i] = $value;
            $paymentDatas[$i]->status = 'success';
            $cekDate[$i] = $value->from_dt;
            $i++;
        }

        // print_r($cekDate);exit;

        foreach ($payments as $key2 => $value2) {
            $cek = in_array($value2->from_dt, $cekDate);
            if($cek == false){
                $paymentDatas[$i] = $value2;
            }
            $i++;
        }

        usort($paymentDatas, array($this,'sortFunction'));

        return $paymentDatas;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            // 'invoice_no' => ['required'],
            // 'user_id' => ['required', 'integer'],
            'total_amt' => ['required', 'between:0,99.99'],
            // 'thn_ajaran' => ['required', 'integer'],
            // 'from_dt' => ['required', 'date'],
            // 'to_dt' => ['required', 'date']
        ]);

        if($request->month && is_array($request->month)){

            DB::beginTransaction();

            try {

                $params = [
                    'bankCd' => $request->bank_cd,
                    'mitraCd' => $request->mitra_cd,
                    'amt' => $request->total_amt,
                    'payMethod' => $request->payment_method
                ];

                $responPG = $this->reqPayment($params);

                if(!$responPG){
                    return ['result' => 'Error in PG', 'code' => '01'];
                }

                $objDemo = new \stdClass();
                $objDemo->resultMsg = $responPG->resultMsg;
                $objDemo->tXid = $responPG->tXid;
                $objDemo->referenceNo = $responPG->referenceNo;
                $objDemo->amt = $responPG->amt;
                $objDemo->bankCd = $this->bankCdToName($responPG->bankCd);
                $objDemo->vacctNo = $responPG->vacctNo;
                $objDemo->billingNm = $responPG->billingNm;
                $objDemo->vacctValidDt = $responPG->vacctValidDt;
                $objDemo->vacctValidTm = $responPG->vacctValidTm;
                $objDemo->receiver = $responPG->billingNm;
                $objDemo->payWay = $this->bankCdToPayWay($responPG->bankCd);
                $objDemo->sender = 'Ariandi Nugraha, db_duabelas@yahoo.com';

                Mail::to("db_duabelas@yahoo.com")->send(new VaCreated($objDemo));
                // print_r($objDemo);exit;

                foreach ($request->month as $key => $value) {
                    if(intval($value) > 6){
                        $thnAjaran = $request->year.' / '.($request->year+1);
                    }else{
                        $thnAjaran = ($request->year-1).' / '.$request->year;
                    }

                    $year = Year::where('year_name', $thnAjaran)->first();
                    if(!$year){
                        $year = new Year();
                        $year->year_name = $thnAjaran;
                        $year->active = 0;
                        $year->insert_by = Auth::user()->id;
                        $year->save();
                    }

                    $insert = [
                        'from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$request->year)),
                        'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$request->year)),
                        'payment_no' => $this->selMaxInv(),
                        'user_id' => Auth::user()->id,
                        'thn_ajaran' => $year->id,
                        'total_amt' => $request->total_amt,
                        'insert_by' => Auth::user()->id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                        'status' => 'pending',
                        'pay_method' => $request->payment_method,
                        'txid' => $responPG->tXid,
                        'vacct_no' => $responPG->vacctNo,
                        'bank_cd' => $request->bank_cd,
                    ];

                    $queryPrep = Payment::where(['from_dt' => date('Y-m-d', strtotime('01-'.$value.'-'.$request->year)),
                                                'to_dt' => date('Y-m-t', strtotime('01-'.$value.'-'.$request->year)),
                                                'user_id' => Auth::user()->id
                                                ]);

                    $invCheck = $queryPrep->count();

                    if($invCheck <= 0){
                        $invInput = DB::table('payments')->insert($insert);
                        $invSel = $queryPrep->first();
                        if($invSel) {
                            if( $request->prod_id && is_array($request->prod_id) ){
                                $invDetRows = [];
                                foreach ($request->prod_id as $key2 => $value2) {
                                    $invDetRows[] = [
                                        'payment_id' => $invSel->id,
                                        'prod_id' => $value2,
                                        'prod_nm' => $request->prod_nm[$key2],
                                        'sale_prc' => $request->sale_prc[$key2],
                                        'insert_by' => Auth::user()->id,
                                        'created_at' => date('Y-m-d H:i:s'),
                                        'updated_at' => date('Y-m-d H:i:s')
                                    ];
                                }
                                $invInputDets = DB::table('payment_dets')->insert($invDetRows);
                            }
                        }
                    }else{
                        return ['result' => 'Error', 'code' => '01'];
                    }
                }

                DB::commit();
                return ['result' => 'Success', 'code' => '00', 'responsePG' => $responPG];
                // all good
            } catch (\Exception $e) {
                DB::rollback();
                // print_r($e->getMessage());die;
                return ['result' => $e, 'code' => '01'];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find(Auth::User()->id);
        if($user){
            $user->fullname = $user->fullname;
            return $user;
        }

        return [];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function selMaxInv()
    {
        $inv = 0;
        $maxInv = Payment::where(DB::RAW('SUBSTR(payment_no, 5,6)'), date('ymd'))
                ->max('payment_no');
                // ->toSql();
        // dd($maxInv);
        if(!$maxInv){
            $inv = 'PAY_'.date('ymd').str_pad("1",4,"0", STR_PAD_LEFT);
        }else{
            $inv = substr($maxInv, 4);
            $inv = $inv+1;
            $inv = 'PAY_'.$inv;
        }
        return $inv;
    }

    public function reqPayment($params)
    {

        $nicepay = new NicepayLib();

        $dateNow        = date('Ymd');
        $vaExpiryDate   = date('Ymd', strtotime($dateNow . ' +1 day')); // Set VA expiry date +1 day (optional)
        $bankCd         = $params['bankCd'];
        $mitraCd         = $params['mitraCd'];
        $timeStamp      = date('Ymdhis');

        // Populate Mandatory parameters to send
        $nicepay->set('timeStamp', $timeStamp); //
        $nicepay->set('referenceNo', $this->selMaxInv()); //
        $nicepay->set('amt', $params['amt']); //
        $nicepay->set('payMethod', $params['payMethod']); //
        $nicepay->set('billingNm', Auth::user()->first_name.' '.Auth::user()->last_name); //
        $nicepay->set('billingPhone', isset(Auth::user()->phone_number) ? Auth::user()->phone_number : '081219836581'); //
        $nicepay->set('billingEmail', 'db_duabelas@yahoo.com'); //
        $nicepay->set('billingAddr', 'Jl. Jend. Sudirman No. 28');
        $nicepay->set('billingCity', 'Jakarta Pusat'); //
        $nicepay->set('billingState', 'DKI Jakarta'); //
        $nicepay->set('billingPostCd', '10210'); //
        $nicepay->set('billingCountry', 'Indonesia'); //

        // $nicepay->set('merFixAcctId', ''); // For Fix Account
        $nicepay->set('currency', 'IDR'); //
        $nicepay->set('description', 'Payment of : '.$this->selMaxInv()); //
        $nicepay->set('vacctValidDt', $vaExpiryDate); //
        $nicepay->set('vacctValidTm', date('His')); //
        ($params['payMethod'] == '02') ? $nicepay->set('bankCd', $bankCd) : $nicepay->set('mitraCd', $mitraCd); //

        // Send Data
        ($params['payMethod'] == '02') ? $response = $nicepay->requestVA() : $nicepay->set('mitraCd', $mitraCd);
        // print_r($response);exit;
        // Response from NICEPAY
        if (isset($response->resultCd))
        {
            return $response;
        }

        return false;
    }

    public function noti(Request $request)
    {
        Log::info('[PaymentController : PaymentController function noti start]');
        $res = '';
        $nicepay = new NicepayLibRedirect();
        //Listen for parameters passed

        $pushParameters = array(
            'tXid',
            'referenceNo',
            'merchantToken',
            'amt',
        );

        Log::info('[PaymentController : PaymentController function noti nicepayLib extractNotification prepare]');
        $nicepay->extractNotification($pushParameters);

        $iMid         = $nicepay->iMid;
        $tXid         = $nicepay->getNotification('tXid');
        $referenceNo  = $nicepay->getNotification('referenceNo');
        $amt          = $nicepay->getNotification('amt');
        $pushedToken  = $nicepay->getNotification('merchantToken');

        $nicepay->set('tXid',$tXid);
        $nicepay->set('amt',$amt);
        $nicepay->set('iMid',$iMid);

        Log::info('[PaymentController : PaymentController function noti prepare get referenceNo]');
        if(isset($referenceNo))
        {
            Log::info('[PaymentController : PaymentController function noti referenceNo is isset]');
            $nicepay->set('referenceNo',$referenceNo);
            $transactionToken = $nicepay->merchantTokenC();
            $nicepay->set('merchantToken',$transactionToken);

            Log::info('[PaymentController : PaymentController function noti nicepayLib checkPaymentStatus prepare]');
            $paymentStatus = $nicepay->checkPaymentStatus($tXid, $referenceNo, $amt);
            // print_r($paymentStatus);exit;
            $response = array(
                    'reqTm'         => $paymentStatus->reqTm,
                    // 'instmntType'   => $paymentStatus->instmntType,
                    'resultMsg'     => $paymentStatus->resultMsg,
                    'reqDt'         => $paymentStatus->reqDt,
                    // 'instmntMon'    => $paymentStatus->instmntMon,
                    'status'        => $paymentStatus->status,
                    'tXid'          => $paymentStatus->tXid
            );

            Log::info('[PaymentController : PaymentController function noti cek pushedToken prepare]');
            Log::info('[PaymentController : '.$pushedToken .'=='. $transactionToken.']');
            if($pushedToken == $transactionToken)
            {
                Log::info('[PaymentController : PaymentController function noti cek pushedToken verify]');
                if(isset($paymentStatus->status) && $paymentStatus->status == '0')
                {
                    $res = "Paid";
                    Log::info('[PaymentController : PaymentController function noti status paid]');

                    DB::beginTransaction();
                    Log::info('[PaymentController : PaymentController function noti beginTransaction]');

                    try {
                        $payment = Payment::where('txid', $paymentStatus->tXid)->first();
                        Log::info('[PaymentController : PaymentController function noti cek payment : '.$payment.']');
                        if($payment){
                            $payment->status = "success";
                            if($payment->save()){
                                Log::info('[PaymentController : PaymentController function noti cek payment saveed]');
                                $queryPrep = Invoices::where(['from_dt' => $payment->from_dt,
                                                            'to_dt' => $payment->to_dt,
                                                            'user_id' => $payment->user_id
                                                            ]);
                                $invCheck = $queryPrep->count();

                                Log::info('[PaymentController : PaymentController function noti cek invoice exist]');
                                if($invCheck <= 0){
                                    $insert = [
                                                'from_dt' => $payment->from_dt,
                                                'to_dt' => $payment->to_dt,
                                                'invoice_no' => $payment->payment_no,
                                                'user_id' => $payment->user_id,
                                                'thn_ajaran' => $payment->thn_ajaran,
                                                'total_amt' => $payment->total_amt,
                                                'insert_by' => $payment->user_id,
                                                'created_at' => date('Y-m-d H:i:s'),
                                                'updated_at' => date('Y-m-d H:i:s'),
                                            ];
                                    $invInput = DB::table('invoices')->insert($insert);
                                    Log::info('[PaymentController : PaymentController function noti input to invoices table]');

                                    $invSel = $queryPrep->first();
                                    if($invSel) {

                                        $payDets = PaymentDet::where('payment_id', $payment->id)->get();
                                        Log::info('[PaymentController : PaymentController function noti get paymentDets]');
                                            $invDetRows = [];
                                            foreach ($payDets as $key2 => $value2) {
                                                $invDetRows[] = [
                                                    'invoice_id' => $invSel->id,
                                                    'prod_id' => $value2->prod_id,
                                                    'prod_nm' => $value2->prod_nm,
                                                    'sale_prc' => $value2->sale_prc,
                                                    'insert_by' => $payment->user_id,
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                    'updated_at' => date('Y-m-d H:i:s')
                                                ];
                                            }
                                            Log::info('[PaymentController : PaymentController function noti input paymentDets]');
                                            $invInputDets = DB::table('invoice_dets')->insert($invDetRows);

                                            $objDemo = new \stdClass();
                                            $objDemo->tXid = $paymentStatus->tXid;
                                            $objDemo->referenceNo = $paymentStatus->referenceNo;
                                            $objDemo->amt = $paymentStatus->amt;
                                            $objDemo->bankCd = $paymentStatus->bankCd;
                                            $objDemo->vacctNo = $paymentStatus->vacctNo;
                                            $objDemo->billingNm = $paymentStatus->billingNm;
                                            $objDemo->receiver = $paymentStatus->billingNm;
                                            $objDemo->sender = 'Ariandi Nugraha, db_duabelas@yahoo.com';

                                            Log::info('[PaymentController : PaymentController function noti send email]');
                                            Mail::to("db_duabelas@yahoo.com")->send(new VaSuccess($objDemo));

                                            Log::info('[PaymentController : PaymentController function noti commit transaction]');
                                            DB::commit();
                                            return ['result' => $res, 'code' => '00'];
                                    }
                                }
                            }
                        }
                    } catch (\Exception $e) {
                        Log::info('[PaymentController : PaymentController function noti rollback transaction]');
                        DB::rollback();
                        return ['result' => $e, 'code' => '01'];
                    }
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '1')
                {
                    $res = "<pre>Reversal</pre>";
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '3')
                {
                    $res = "<pre>Cancel</pre>";
                }
                elseif (isset($paymentStatus->status) && $paymentStatus->status == '4')
                {
                    $res = "<pre>Expired</pre>";
                }
                else
                {
                    $res = "<pre>Status Unknown</pre>";
                }
            }
        }
        return $res;
    }

    private function sortFunction( $a, $b ) {
        if (strtotime($a->from_dt) < strtotime($b->from_dt))
            return 1;
        else if (strtotime($a->from_dt) > strtotime($b->from_dt))
            return -1;
        else
            return 0;
        // return strtotime($a->from_dt) - strtotime($b->from_dt);
    }

    private function bankCdToName($bankCd){
        $bankName = '';
        if($bankCd == 'BMRI'){
          $bankName = 'Bank Mandiri';
        }else if($bankCd == 'BNIN'){
          $bankName = 'BNI 46';
        }else if($bankCd == 'BRIN'){
          $bankName = 'BRI';
        }else if($bankCd == 'CENA'){
          $bankName = 'BCA';
        }else if($bankCd == 'BBBA'){
          $bankName = 'Bank Permata';
        }else if($bankCd == 'BNIA'){
          $bankName = 'Bank CIMB Niaga';
        }else if($bankCd == 'BDIN'){
          $bankName = 'Bank Danamon';
        }

        return $bankName;
      }

    private function bankCdToPayWay($bankCd){
        $content = '';
        if($bankCd == 'BMRI'){
          $content = '<h3>ATM Mandiri</h3>';
          $content .= '<ul>';
          $content .= '<li>Input kartu ATM dan PIN Anda</li>';
          $content .= '<li>Pilih Menu Bayar/Beli</li>';
          $content .= '<li>Pilih Lainnya</li>';
          $content .= '<li>Pilih Multi Payment</li>';
          $content .= '<li>Input 70014 sebagai Kode Institusi</li>';
          $content .= '<li>Input Virtual Account Number, misal. 70014XXXXXXXXXXX</li>';
          $content .= '<li>Pilih Benar</li>';
          $content .= '<li>Pilih Ya</li>';
          $content .= '<li>Pilih Ya</li>';
          $content .= '<li>Ambil bukti bayar Anda</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking Mandiri</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Mobile Banking</li>';
          $content .= '<li>Pilih Bayar</li>';
          $content .= '<li>Pilih Multi Payment</li>';
          $content .= '<li>Input Transferpay sebagai Penyedia Jasa</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>';
          $content .= '<li>Pilih Lanjut</li>';
          $content .= '<li>Input OTP and PIN</li>';
          $content .= '<li>Pilih OK</li>';
          $content .= '<li>Bukti bayar ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Internet Banking Mandiri</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Internet Banking</li>';
          $content .= '<li>Pilih Bayar</li>';
          $content .= '<li>Pilih Multi Payment</li>';
          $content .= '<li>Input Transferpay sebagai Penyedia Jasa</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX sebagai Kode Bayar</li>';
          $content .= '<li>Ceklis IDR</li>';
          $content .= '<li>Klik Lanjutkan</li>';
          $content .= '<li>Bukti bayar ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';
        }else if($bankCd == 'BNIN'){
          $content = '<h3>ATM BNI</h3>';
          $content .= '<ul>';
          $content .= '<li>Masukkan Kartu Anda.</li>';
          $content .= '<li>Pilih Bahasa</li>';
          $content .= '<li>Masukkan PIN ATM Anda.</li>';
          $content .= '<li>Pilih Menu Lainnya.</li>';
          $content .= '<li>Pilih Transfer.</li>';
          $content .= '<li>Pilih Jenis rekening yang akan Anda gunakan (Contoh; Dari Rekening Tabungan).</li>';
          $content .= '<li>Pilih Virtual Account Billing</li>';
          $content .= '<li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX).</li>';
          $content .= '<li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
          $content .= '<li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li>';
          $content .= '<li>Transaksi Anda telah selesai.</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking BNI</h3>';
          $content .= '<ul>';
          $content .= '<li>Akses BNI Mobile Banking dari handphone kemudian masukkan user ID dan password.</li>';
          $content .= '<li>Pilih menu Transfer.</li>';
          $content .= '<li>Pilih menu Virtual Account Billing kemudian pilih rekening debet.</li>';
          $content .= '<li>Masukkan Nomor Virtual Account Anda (contoh: 123456789012XXXX) pada menu input baru.</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 70014XXXXXXXXXXX</li>';
          $content .= '<li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
          $content .= '<li>Konfirmasi transaksi dan masukkan Password Transaksi.</li>';
          $content .= '<li>Pembayaran Anda Telah Berhasil.</li>';
          $content .= '</ul>';

          $content .= '<h3>iBank Personal BNI</h3>';
          $content .= '<ul>';
          $content .= '<li>Ketik alamat https://ibank.bni.co.id kemudian klik Enter.</li>';
          $content .= '<li>Masukkan User ID dan Password.</li>';
          $content .= '<li>Pilih menu Transfer.</li>';
          $content .= '<li>Pilih Virtual Account Billing.</li>';
          $content .= '<li>Kemudian masukan Nomor Virtual Account Anda (contoh: 123456789012XXXX) yang hendak dibayarkan. Lalu pilih rekening debet yang akan digunakan. Kemudian tekan lanjut</li>';
          $content .= '<li>Kemudin tagihan yang harus dibayarkan akan muncul pada layar konfirmasi.</li>';
          $content .= '<li>Masukkan Kode Otentikasi Token.</li>';
          $content .= '<li>Pembayaran Anda telah berhasil.</li>';
          $content .= '</ul>';
        }else if($bankCd == 'BRIN'){
          $content = '<h3>ATM BRI</h3>';
          $content .= '<ul>';
          $content .= '<li>Input kartu ATM dan PIN Anda</li>';
          $content .= '<li>Pilih Menu Transaksi Lain</li>';
          $content .= '<li>Pilih Menu Pembayaran</li>';
          $content .= '<li>Pilih Menu Lain-lain</li>';
          $content .= '<li>Pilih Menu BRIVA</li>';
          $content .= '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
          $content .= '<li>Pilih Ya</li>';
          $content .= '<li>Ambil bukti bayar anda</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking BRI</h3>';
          $content .= '<ul>';
          $content .= '<li>Login BRI Mobile</li>';
          $content .= '<li>Pilih Mobile Banking BRI</li>';
          $content .= '<li>Pilih Menu Pembayaran</li>';
          $content .= '<li>Pilih Menu BRIVA</li>';
          $content .= '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
          $content .= '<li>Masukkan Nominal misal. 10000</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Masukkan PIN Mobile</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar akan dikirim melalui sms</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Internet Banking BRI</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Internet Banking</li>';
          $content .= '<li>Pilih Pembayaran</li>';
          $content .= '<li>Pilih BRIVA</li>';
          $content .= '<li>Masukkan Nomor Virtual Account, misal. 88788XXXXXXXXXXX</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Masukkan Password</li>';
          $content .= '<li>Masukkan mToken</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar akan ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';
        }else if($bankCd == 'CENA'){
          $content = '<h3>ATM BCA</h3>';
          $content .= '<ul>';
          $content .= '<li>Input kartu ATM dan PIN Anda</li>';
          $content .= '<li>Pilih Menu Transaksi Lainnya</li>';
          $content .= '<li>Pilih Transfer</li>';
          $content .= '<li>Pilih Ke rekening BCA Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 123456789012XXXX</li>';
          $content .= '<li>Pilih Benar</li>';
          $content .= '<li>Pilih Ya</li>';
          $content .= '<li>Ambil bukti bayar Anda</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking BCA</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Mobile Banking</li>';
          $content .= '<li>Pilih m-Transfer</li>';
          $content .= '<li>Pilih BCA Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>';
          $content .= '<li>Klik Send</li>';
          $content .= '<li>Informasi Virtual Account akan ditampilkan</li>';
          $content .= '<li>Klik OK</li>';
          $content .= '<li>Input PIN Mobile Banking</li>';
          $content .= '<li>Bukti bayar ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Internet Banking BCA</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Internet Banking</li>';
          $content .= '<li>Pilih Transaksi Dana</li>';
          $content .= '<li>Pilih Transfer Ke BCA Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 123456789012XXXX sebagai No. Virtual Account</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Input Respon KeyBCA Appli 1</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';
        }else if($bankCd == 'BBBA'){
          $content = '<h3>ATM Permata</h3>';
          $content .= '<ul>';
          $content .= '<li>Input kartu ATM dan PIN Anda</li>';
          $content .= '<li>Pilih Menu Transaksi Lainnya</li>';
          $content .= '<li>Pilih Pembayaran</li>';
          $content .= '<li>Pilih Pembayaran Lain-lain</li>';
          $content .= '<li>Pilih Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX</li>';
          $content .= '<li>Select Benar</li>';
          $content .= '<li>Select Ya</li>';
          $content .= '<li>Ambil bukti bayar anda</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking Permata</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Mobile Banking</li>';
          $content .= '<li>Pilih Pembayaran Tagihan</li>';
          $content .= '<li>Pilih Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX sebagai No. Virtual Account</li>';
          $content .= '<li>Input Nominal misal. 10000</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Input Token</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Internet Banking BCA</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Internet Banking</li>';
          $content .= '<li>Pilih Pembayaran Tagihan</li>';
          $content .= '<li>Virtual Account</li>';
          $content .= '<li>Input Nomor Virtual Account, misal. 8625XXXXXXXXXXXX sebagai No. Virtual Account</li>';
          $content .= '<li>Input Nominal misal. 10000</li>';
          $content .= '<li>Input Token</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar akan ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';
        }else if($bankCd == 'BNIA'){
          $content = '<h3>ATM CIMB Niaga</h3>';
          $content .= '<ul>';
          $content .= '<li>Input kartu ATM dan PIN Anda</li>';
          $content .= '<li>Pilih Menu Pembayaran</li>';
          $content .= '<li>Pilih Menu Lanjut</li>';
          $content .= '<li>Pilih Menu Virtual Account</li>';
          $content .= '<li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>';
          $content .= '<li>Pilih Proses</li>';
          $content .= '<li>Data Virtual Account akan ditampilkan</li>';
          $content .= '<li>Pilih Proses</li>';
          $content .= '<li>Ambil bukti bayar anda</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Mobile Banking CIMB Niaga</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Go Mobile</li>';
          $content .= '<li>Pilih Menu Transfer</li>';
          $content .= '<li>Pilih Menu Transfer ke CIMB Niaga Lain</li>';
          $content .= '<li>Pilih Sumber Dana yang akan digunakan</li>';
          $content .= '<li>Masukkan Nomor Virtual Account, misal. 5919XXXXXXXXXXXX</li>';
          $content .= '<li>Masukkan Nominal misal. 10000</li>';
          $content .= '<li>Klik Lanjut</li>';
          $content .= '<li>Data Virtual Account akan ditampilkan</li>';
          $content .= '<li>Masukkan PIN Mobile</li>';
          $content .= '<li>Klik Konfirmasi</li>';
          $content .= '<li>Bukti bayar akan dikirim melalui sms</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';

          $content .= '<h3>Internet Banking CIMB Niaga</h3>';
          $content .= '<ul>';
          $content .= '<li>Login Internet Banking</li>';
          $content .= '<li>Pilih Pembayaran Tagihan</li>';
          $content .= '<li>Rekening Sumber - Pilih yang akan Anda digunakan</li>';
          $content .= '<li>Jenis Pembayaran - Pilih Virtual Account</li>';
          $content .= '<li>Untuk Pembayaran - Pilih Masukkan Nomor Virtual Account</li>';
          $content .= '<li>Nomor Rekening Virtual, misal. 5919XXXXXXXXXXXX</li>';
          $content .= '<li>Isi Remark Jika diperlukan</li>';
          $content .= '<li>Klik Lanjut</li>';
          $content .= '<li>Data Virtual Account akan ditampilkan</li>';
          $content .= '<li>Masukkan mPIN</li>';
          $content .= '<li>Klik Kirim</li>';
          $content .= '<li>Bukti bayar akan ditampilkan</li>';
          $content .= '<li>Selesai</li>';
          $content .= '</ul>';
        }else if($bankCd == 'BDIN'){
          $content = '<h3>ATM Danamon</h3>';
          $content .= '<ul>';
          $content .= '<li>Input PIN ATM Anda</li>';
          $content .= '<li>Pilih Menu Pembayaran >>> Virtual Account Masukan nomor Virtual Account</li>';
          $content .= '<li>Masukkan Nominal</li>';
          $content .= '<li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi</li>';
          $content .= '</ul>';

          $content .= '<h3>Aplikasi D-Mobile Danamon</h3>';
          $content .= '<ul>';
          $content .= '<li>Login pada Aplikasi D-Mobile</li>';
          $content .= '<li>Pilih menu Virtual Account</li>';
          $content .= '<li>Masukan 16 digit nomor virtual account</li>';
          $content .= '<li>Masukan Nominal</li>';
          $content .= '<li>Pada layar konfirmasi pembayaran, pastikan transaksi sudah benar -> pilih Ya untuk memproses transaksi.</li>';
          $content .= '</ul>';
        }

        return $content;
      }
}
