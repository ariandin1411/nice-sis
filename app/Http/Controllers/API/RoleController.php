<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\Role;
use App\Models\UserRole;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Role::where('active', 1);
        
        $perPage = 10;
        if(\Request::get('perPage')){
            $perPage = \Request::get('perPage');
        }

        if($cari = \Request::get('cariNis')){
            $role = $role->where(function($query) use ($cari) {
                $query->where('role_name','LIKE', "%$cari%");
            });
        }

        $role = $role->latest()->paginate($perPage);

        if(\Request::get('user_id')){
            $roleUser = UserRole::select('user_roles.id', 'u.id AS user_id', 
                                        'r.id AS role_id', 'r.role_name')
                ->join('users AS u', 'u.id', '=', 'user_roles.user_id')
                ->join('roles AS r', 'r.id', '=', 'user_roles.role_id')
                ->where('u.id', \Request::get('user_id'))
                ->get();
            
            $getUserRoleId = [];
            
            foreach ($roleUser as $key => $value) {
                $getUserRoleId[] = $value->role_id;
            }

            $role = Role::whereNotIn('id', $getUserRoleId)->latest()->paginate($perPage);
        }

        return $role;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role_name' => ['required', 'string', 'max:191'],
            'level' => ['nullable', 'integer', 'max:191'],
            'description' => ['required', 'string', 'max:191'],
        ]);

        return Role::create([
            'role_name' => $request->role_name,
            'level' => $request->level,
            'description' => $request->description,
            'active' => 1,
            'insert_by' => Auth::user()->id,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'role_name' => ['required', 'string', 'max:191'],
            'level' => ['nullable', 'integer', 'max:191'],
            'description' => ['required', 'string', 'max:191'],
        ]);

        $role = Role::findOrFail($id);
        $input = $request->all();
        $input['update_by'] = Auth::user()->id;

        $role->update($input);
        return ['message' => 'Updated role info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');
        
        $Role = Role::findOrFail($id);

        $Role->delete();

        return ['message' => 'Role Deleted'];
    }
}
