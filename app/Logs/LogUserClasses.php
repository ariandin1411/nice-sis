<?php

namespace App\Logs;

use Illuminate\Database\Eloquent\Model;

class LogUserClasses extends Model
{
    protected $fillable = ['user_classes_id', ' user_id', 'from_classes_id', 'to_classes_id', ' insert_by'];
}
