<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use DB;

class UsersExport implements FromView, WithColumnFormatting, WithEvents
{
    public function view(): View
    {
        $teacherData = self::query()->latest()->get();
        $data = ['title' => 'SMA N 2 Cibinong', 'teacherData' => $teacherData];
        return view('users.user-excel-view', $data);
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => [self::class, 'afterSheet']
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $teacherData = self::query()->count()+3;

        $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(8);
        $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('c')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(15);
        $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(15);
        $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(15);
        $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(20);

        $event->sheet->getStyle('A1:H1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $event->sheet->getDelegate()->getStyle('A1:H1')->getFont()->setSize(14);
        $event->sheet->getDelegate()->getStyle('A3:H3')->getFont()->setSize(12);

        $event->sheet->getStyle('A3:H3')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('CCCCCC');

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $event->sheet->getStyle('A3:H'.$teacherData)->applyFromArray($styleArray);
        // $event->sheet->getCellValueExplicit('B',
        //     "01513789642",
        //     \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        // );
        // $event->sheet->styleCells( 'A1',['alignment' => ['horizontal' => ]] );
    }

    private static function query() {
        if($search = \Request::get('q')){
            $users = User::where(['type' => 'T', 'active' => 1])
                ->where(function($query) use ($search){
                    $query->where(DB::raw('CONCAT(`first_name`, " ", `last_name`)'), 'LIKE', "%$search%")
                    ->orWhere('email', 'LIKE', "%$search%")
                    // ->orWhere('nis', "$search")
                    ->orWhere('nig', "$search");
            });
        }else{
            $users = User::where(['type' => 'T', 'active' => 1]);
        }

        return $users;
    }
}
