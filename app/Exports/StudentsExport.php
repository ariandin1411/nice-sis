<?php

namespace App\Exports;

use App\User;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use DB;

class StudentsExport implements FromView, WithColumnFormatting, WithEvents
{
    public function view(): View
    {
        $studentsData = self::query()->orderBy(DB::RAW("kelasdet.class_name, kelasdet.major, 
            kelasdet.class_sub_name, users.first_name, users.last_name"))->get();
        $data = ['title' => 'SMA N 2 Cibinong', 'studentsData' => $studentsData];
        return view('users.students-excel-view', $data);
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => [self::class, 'afterSheet']
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $teacherData = self::query()->count()+3;

        $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(8);
        $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('c')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(30);
        $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('F')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('G')->setWidth(15);
        $event->sheet->getDelegate()->getColumnDimension('H')->setWidth(20);
        $event->sheet->getDelegate()->getColumnDimension('I')->setWidth(20);

        $event->sheet->getStyle('A1:I1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $event->sheet->getDelegate()->getStyle('A1:I1')->getFont()->setSize(14);
        $event->sheet->getDelegate()->getStyle('A3:I3')->getFont()->setSize(12);

        $event->sheet->getStyle('A3:I3')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('CCCCCC');

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $event->sheet->getStyle('A3:I'.$teacherData)->applyFromArray($styleArray);
        // $event->sheet->getCellValueExplicit('B',
        //     "01513789642",
        //     \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        // );
        // $event->sheet->styleCells( 'A1',['alignment' => ['horizontal' => ]] );
    }

    private static function query() {
        $users = User::select('users.id', 'users.first_name', 'users.last_name', 'users.nis', 'users.gender', 'users.email',
                            'users.address', 'users.phone_number', 'users.year_of_entry', 'kelas.id AS kelas_id',
                            'kelasdet.class_name', 'kelasdet.class_sub_name', 'kelasdet.major',
                            'users.created_at', 'users.birth_date',
                            'users.born_at', 'users.religion', 'users.nisn')
                ->leftJoin('user_classes AS kelas', 'kelas.user_id', '=', 'users.id')
                ->leftJoin('classes AS kelasdet', 'kelas.class_id', '=', 'kelasdet.id')
                ->where(['users.type' => 'S', 'users.active' => 1]);

        if($cari = \Request::get('cariKelas')){
            if($cari != 0){
                $users = $users->where('kelasdet.id', $cari);
            }
        }

        return $users;
    }
}
