<?php

namespace App\Exports;

use App\Models\TeacherClass;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use DB;

class TeacherClassesExport implements FromView, WithColumnFormatting, WithEvents
{
    public function view(): View
    {
        // $teacherClass = DB::table('teacher_classes AS a')
        //         ->select('a.*', 'b.first_name', 'b.last_name', 'c.class_name', 'c.class_sub_name', 'd.year_name', 
        //                 'c.major', 'b.nig')
        //         ->leftJoin('users AS b', 'a.user_id', '=', 'b.id')
        //         ->leftJoin('classes AS c', 'a.class_id', '=', 'c.id')
        //         ->leftJoin('years AS d', 'a.year_id', '=', 'd.id')
        //         ->where(['a.active' => 1, 'b.active' => 1, 'c.active' => 1, 'd.active' => 1, 'b.type' => 'T']);
        
        // if($cariKelas = \Request::get('cariKelas')){
        //     $teacherClass = $teacherClass->where('a.class_id', $cariKelas);
        // }

        // if($cariGuru = \Request::get('cariGuru')){
        //     $teacherClass = $teacherClass->where(function($query) use($cariGuru){
        //         $query->where(DB::raw('CONCAT(b.first_name, " ", b.last_name)'), 'LIKE', "%$cariGuru%")
        //                             ->orWhere('b.email', 'LIKE', "%$cariGuru%")
        //                             ->orWhere('b.nig', '=', "$cariGuru");
        //     });
        // }

        $teacherClass = self::query()->latest()->get();

        $data = ['title' => 'SMA N 2 Cibinong', 'teacherData' => $teacherClass];
        return view('teacherclasses.teacherclasses-excel-view', $data);
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_TEXT,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => [self::class, 'afterSheet']
        ];
    }

    public static function afterSheet(AfterSheet $event)
    {
        $teacherData = self::query()->count()+3;

        $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(8);
        $event->sheet->getDelegate()->getColumnDimension('B')->setWidth(25);
        $event->sheet->getDelegate()->getColumnDimension('c')->setWidth(30);
        $event->sheet->getDelegate()->getColumnDimension('D')->setWidth(15);
        $event->sheet->getDelegate()->getColumnDimension('E')->setWidth(15);

        $event->sheet->getStyle('A1:E1')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
        $event->sheet->getDelegate()->getStyle('A1:E1')->getFont()->setSize(14);
        $event->sheet->getDelegate()->getStyle('A3:E3')->getFont()->setSize(12);

        $event->sheet->getStyle('A3:E3')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB('CCCCCC');

        $styleArray = [
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                    'color' => ['argb' => '000000'],
                ],
            ],
        ];

        $event->sheet->getStyle('A3:E'.$teacherData)->applyFromArray($styleArray);
        // $event->sheet->getCellValueExplicit('B',
        //     "01513789642",
        //     \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
        // );
        // $event->sheet->styleCells( 'A1',['alignment' => ['horizontal' => ]] );
    }

    private static function query() {
        $teacherClass = DB::table('teacher_classes AS a')
                ->select('a.*', 'b.first_name', 'b.last_name', 'c.class_name', 'c.class_sub_name', 'd.year_name', 
                        'c.major', 'b.nig')
                ->leftJoin('users AS b', 'a.user_id', '=', 'b.id')
                ->leftJoin('classes AS c', 'a.class_id', '=', 'c.id')
                ->leftJoin('years AS d', 'a.year_id', '=', 'd.id')
                ->where(['a.active' => 1, 'b.active' => 1, 'c.active' => 1, 'd.active' => 1, 'b.type' => 'T']);
        
        if($cariKelas = \Request::get('cariKelas')){
            $teacherClass = $teacherClass->where('a.class_id', $cariKelas);
        }

        if($cariGuru = \Request::get('cariGuru')){
            $teacherClass = $teacherClass->where(function($query) use($cariGuru){
                $query->where(DB::raw('CONCAT(b.first_name, " ", b.last_name)'), 'LIKE', "%$cariGuru%")
                                    ->orWhere('b.email', 'LIKE', "%$cariGuru%")
                                    ->orWhere('b.nig', '=', "$cariGuru");
            });
        }

        return $teacherClass;
    }
}
