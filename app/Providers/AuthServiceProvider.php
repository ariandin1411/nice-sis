<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;
use App\Models\Role;
use App\Models\UserRole;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('isAdmin', function($user){
            $userRole = UserRole::where('user_id', $user->id);
            $userRoleCount = $userRole->count();
            if($userRoleCount > 0){
                $userRole = $userRole->first();
                $role = Role::findOrFail($userRole->role_id);
                return $role->role_name === 'admin';
            }
        });

        Gate::define('isTeacher', function($user){
            $userRole = UserRole::where('user_id', $user->id);
            $userRoleCount = $userRole->count();
            if($userRoleCount > 0){
                $userRole = $userRole->first();
                $role = Role::findOrFail($userRole->role_id);
                return $role->role_name === 'guru';
            }
        });

        Gate::define('isStudent', function($user){
            $userRole = UserRole::where('user_id', $user->id);
            $userRoleCount = $userRole->count();
            if($userRoleCount > 0){
                $userRole = $userRole->first();
                $role = Role::findOrFail($userRole->role_id);
                return $role->role_name === 'siswa';
            }
        });

        Passport::routes();
        //
    }
}
