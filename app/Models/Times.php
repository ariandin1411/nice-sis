<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Times extends Model
{
    protected $fillable = ['time_name', 'from_time', 'to_time', 'time_interval', 'active', 'insert_by', 'update_by'];
}
