<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['payment_no', 'user_id', 'total_amt', 'thn_ajaran', 
                          'from_dt', 'to_dt', 'insert_by', 'update_by', 'status', 
                          'pay_method', 'txid', 'vacct_no', 'bank_cd'];

    public function dets()
    {
        return $this->hasMany('App\Models\PaymentDet');
    }

    public function years()
    {
        return $this->belongsTo('App\Models\Year', 'thn_ajaran');
    }
}
