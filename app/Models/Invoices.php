<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $fillable = ['invoice_no', 'user_id', 'total_amt', 'thn_ajaran', 
                          'from_dt', 'to_dt', 'insert_by', 'update_by'];

    public function dets()
    {
        return $this->hasMany('App\Models\InvoiceDets');
    }

    public function years()
    {
        return $this->belongsTo('App\Models\Year', 'thn_ajaran');
    }
}
