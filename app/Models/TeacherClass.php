<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherClass extends Model
{
    protected $fillable = ['user_id','class_id','year_id','active','insert_by','update_by'];
}
