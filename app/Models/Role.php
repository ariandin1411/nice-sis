<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['role_name', 'level', 'description', 'active', 'insert_by', 'update_by'];
}
