<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentDet extends Model
{
    protected $fillable = ['payment_id', 'prod_id', 'prod_nm', 'sale_prc', 'insert_by', 'update_by'];
}
