<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scores extends Model
{
    protected $fillable = [
                            'user_id', 'subject_id', 'teacher_id', 'score', 'active', 'insert_by', 'update_by', 'year_id', 
                            'semester'
                          ];
}
