<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = ['company_number', 'company_name', 
                            'website','email','phone1','phone2','phone3','active','province','city','district','address','logo','classification','bank_account_name','bank_name','bank_account','active','insert_by','update_by'
                          ];
}
