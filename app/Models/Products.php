<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['product_no', 'product_name', 'company_id', 'buy_price', 
                          'sale_price', 'tax', 'insert_by', 'update_by', 'active'];
}
