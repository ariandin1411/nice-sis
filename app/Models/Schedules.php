<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Schedules extends Model
{
    protected $fillable = [
                            'subject', 'user_id', 'time', 'day_name', 'schedule_type', 
                            'year_id', 'semester', 'class_id', 'active', 'insert_by', 'update_by'
                          ];
}
