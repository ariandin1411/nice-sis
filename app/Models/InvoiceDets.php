<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InvoiceDets extends Model
{
    protected $fillable = ['invoice_id', 'prod_id', 'prod_nm', 'sale_prc', 'insert_by', 'update_by'];
}
