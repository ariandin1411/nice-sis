<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserParent extends Model
{
    protected $fillable = ['user_id', 'parent_mother', 'parent_father', 'phone_number', 'active', 'insert_by', 'update_by'];
}
