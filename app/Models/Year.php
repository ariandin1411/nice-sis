<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Year extends Model
{
    protected $fillable = ['year_name', 'active', 'insert_by', 'update_by'];
}
