<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presents extends Model
{
    protected $fillable = ['user_id', 'desc', 'year_id', 'semester', 'active', 'insert_by', 'update_by', 'date_in'];
}
