<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Models\Role;
use App\Models\UserRole;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password','first_name', 'mid_name', 'last_name', 
        'nig', 'nis','type','gender','address','phone_number','phone_number2','year_of_entry',
        'active','insert_date','insert_by','update_date','update_by', 'profile_pic', 'role',
        'birth_date', 'born_at', 'religion', 'nisn'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getRoleAttribute() {
        $userRole = UserRole::where('user_id', $this->id);
        $userRoleCount = $userRole->count();
        if($userRoleCount > 0){
            $userRole = $userRole->first();
            $role = Role::findOrFail($userRole->role_id);
            if($role->role_name == 'admin'){
                return 'admin';
            }elseif ($role->role_name == 'guru') {
                return 'guru';
            }elseif ($role->role_name == 'siswa') {
                return 'siswa';
            }

            return '';
        }
    }

    public function getFullnameAttribute() {
        return $this->first_name.' '.$this->last_name;
    }

}
